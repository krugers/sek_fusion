\section{Straight-Field Line Coordinate System}
\setcounter{equation}{0}
%\renewcommand{\theequation}{B.\arabic{equation}}
\label{SFA}

This is from Appendix B in my thesis.  It is meant to be the briefest
introduction to straight-field-line coordinate systems possible; specifically,
it tries to introduce contra-variant and co-variant vectors in two pages.  

More details about straight-field-line coordinate systems, and the
generalizations, can be found in 
S.E. Kruger and J.M. Greene, {\em The relationship between flux coordinates
    and equilibrium-based frames of reference in fusion theory}, Phys. Plasmas
    {\bf 26}, 082506 (2019); \url{https://doi.org/10.1063/1.5098313}
The useful tables from that paper follow.

All coordinate systems are based on reference to surfaces of constant scalar functions.  
For example, the cylindrical coordinate system $(r, \theta, z)$,
is based on surfaces of constant radius, polar angle, and azimuthal coordinate respectively.  The familiar
coordinate basis vectors of this system, $\hat{r}, \hat{\theta}, \hat{z}$, are unit vectors that are perpendicular 
to these surfaces.  Similarly, cartesian basis vectors are unit vectors that are perpendicular to the $x, y, z$ planes.

To generalize these ideas, we consider a generalized system of $\rho, \Theta, \zeta$ surfaces.  At this point,
these surfaces are arbitrary.  We introduce the contravariant basis vectors which are perpendicular
to these surfaces:
\begin{equation}
	\vur = \gr,\ \ \ \ \vut = \gt,\ \ \ \ \vuz = \gz.
\end{equation}
Note that these basis vectors, which are analagous to the more familiar cylindrical coordinate vectors, are not
dimensionless nor are they necessarily orthogonal to each other.   We may also define a set of basis vectors which
are parallel to the curve formed by the intersection of two coordinate surfaces.  These are the covariant 
basis vectors:
\begin{equation}
	\vlr = \jac \gt \times \gz,\ \ \ \ \vlt = \jac \gz \times \gr,\ \ \ \ \vlz = \jac \gr \times \gt.
\end{equation}
where $\jac \equiv \vlr \cdot \vlt \times \vlz$ is the Jacobian of the coordinate system.  One can show that 
in an equivalent way, $\jaci \equiv \vur \cdot \vut \times \vuz$.  The Jacobian is used in the 
definition of the covariant basis vectors in order for the two
representations to be orthonormal; i.e.,
\begin{equation}
\label{ortho}
	\ve^i \cdot \ve_j = \delta^i_j
\end{equation}
where $\delta^i_j$ is the Kronecker delta.

Any vector is represented in terms of these basis vectors by the following representation
\begin{eqnarray}
\label{representation}
	\vA &=& A^i \ve_i   \nonumber \\
	    &=& A_i \ve^i,
\end{eqnarray}
where the Einstein notation that repeated indices are implicitly summed over is adopted
(i.e., $A^i \ve_i = \sum A^i \ve_i,\ i \in \{\R, \T, \Z \}$).  The ${A^i}$'s are referred to as the contravariant
components and the $A_i$'s are referred to as the covariant components.  By 
Eqs.\ (\ref{ortho}) and (\ref{representation}), the components may
clearly be seen to be given by $A^i = \vA \cdot \ve^i$ and $A_i = \vA \cdot \ve_i$.  

The choice of which set of basis vectors to use in representing 
a vector depends upon the type of operation one wishes to perform on the vector.  For example, the gradient
of a scalar function is most easily represented using contravariant basis vectors:
\begin{equation}
	\grad f = \dr{f} \vur + \dT{f} \vut + \dz{f} \vuz.
\end{equation}
The identity $\dive{\grad f \times \grad g} = 0$ implies that the divergence of a vector is most easily 
calculated when the covariant basis vectors are used, and the identity $\curl{\grad f} = 0$ implies that the
curl of a vector is most easily calculated when the contravariant basis vectors are used. 

Because different operations are easier according to which base vector set is used, 
an easy way to transform between the
two representations is needed.  This may be accomplished by defining the metric tensors as
\begin{equation}
	 g^{ij} = \ve^i \cdot \ve^j,\ \ \ \ g_{ij} = \ve_i \cdot \ve_j.
\end{equation}
where $g^{ij}$ is referred to as the contravariant metric tensor, and $g_{ij}$ is referred to as the covariant metric tensor.
Using the above definition and Eq.\ (\ref{representation}), we see that 
we can convert vector components through the relations
\begin{eqnarray} 
	\vA \cdot \ve_j = A_j = A^i g_{ij},    \nonumber \\
	\vA \cdot \ve^j = A^j = A_i g^{ij},
\end{eqnarray}
and similarly for the basis vectors themselves
\begin{eqnarray} 
\label{vegij}
	\ve_j = \ve^i g_{ij},    \nonumber \\
	\ve^j = \ve_i g^{ij}.
\end{eqnarray}
Note that the first relation allows us to write $A_j = A^i g_{ij} = (A_k g^{ik}) g_{ij}$ which implies
\begin{equation}
	g^{ik} g_{ij}  = \delta^i_k.
\end{equation}
Or, referring to the tensors as matrices, one would say that the contravariant metric matrix is the inverse of the 
covariant metric matrix.  Also note that applying Eq.\ (\ref{vegij}) to our definition of the 
Jacobian allows us to write
\begin{equation} 
\label{jacrel}
	\jac^{2} = \mid \mid g_{ij} \mid \mid,\ \ \ \ \ \ \jac^{-2} = \mid \mid g^{ij} \mid \mid.
\end{equation}
Thus, the Jacobian is just the square root of the determinant of the covariant metric matrix.


We now want to be more specific in our choice of the $\rho, \Theta, \zeta$
surfaces by basing them on a toroidal magnetic system.  Our first choice is to
choose $\R$ to be a flux coordinate; i.e., 
\begin{equation}
	\vB_0 \cdot \gr = 0.
\end{equation}
Because of the importance of distinguishing between variations parallel to the magnetic field lines 
and variations perpendicular to 
the magnetic field line, this makes an obvious choice for a radial-like coordinate.

%\begin{figure}
%     \includegraphics[width=0.9\columnwidth]{Coordinate7.pdf} \hfill
%        \caption{The magnetic-flux coordinates used in the text are based on
%                surfaces of constant $\psi, \Theta$, and $\zeta$. }
%        \label{CoordFig}
%\end{figure}

The second choice is to let $\Z$ be the axisymmetric torodial angle.  If we denote the cylindrical coordinate system 
describing the same toroidal system as $(R, \phi, Z)$, then $\zeta = -\phi$.  The advantage of this choice is that 
the coordinate system will now be partially orthogonal: $\gr \cdot \gz =0,\ \gt \cdot \gz = 0$.  This simplifies the 
contravariant metric tensor to
\begin{equation}
	g^{ij} =\lp \begin{array}{ccc}
		\grr & \grt & 0  \\
		\grt & \gtt & 0  \\
		 0   &  0   &  \gzz.
	\end{array}
	\rp
\end{equation}
Using the fact that $\gz \cdot \gz = 1/R^2$, and Eq.\ (\ref{jacrel}), 
we can write $\jac^{-2} = (\grr \grt - {\grt}^2)/R^2$.
Taking the inverse of the above matrix gives the covariant metric tensor with the non-zero elements
\begin{equation}
	g_{\R \R} = {\jac^2 \gtt \over R^2},\ \ g_{\R \T} = {- \jac^2 \grt \over R^2},\ \ 
	g_{\T \T} = {\jac^2 \grr \over R^2},\ \ g_{\Z \Z} =  R^2.
\end{equation}

The magnetic field for an axisymetric system can be written in terms of covariant
basis vectors as
\begin{eqnarray}
	B_0 &=& I(\psi) \gz + \gz \times \grad \psi  \nonumber \\
	    &=& {I \over R^2} \ve_{\Z} +  \dsdr \jaci \ve_{\T}.
\end{eqnarray}
where $\dsdr = d\psi/d\rho$.
Using metric elements to relate the components, we can see that the covariant 
and contravariant components of the magnetic field are
\begin{eqnarray}
\label{BComponents}
	B^{\Z} = {I \over R^2}, & B^{\T} = \dsdr \jac,  & B^{\R} = 0,   \nonumber \\
	{B_{\Z}} = I, 		& {B_{\T}} = {\dsdr \jac \over R^2} \grr,  
	& {B_{\R}}= {- \dsdr \jac \over R^2} \grt.
\end{eqnarray}

The final variable that is specified is $\Theta$, which is chosen to make the ratio of contravariant magnetic 
field components a flux function:
\begin{equation}
\label{StraightField}
	{B^{\Z} \over B^{\T} } = q(\rho)
\end{equation}
where $q$ is the safety factor.  This type of coordinate system, called 
a ``straight-field line coordinate system," 
has the advantage of making the $\vB_0 \cdot \grad$ operator zero whenever $q$ is a rational
number:
\begin{equation}
	\vB_0 \cdot \grad \Qt e^{im\T -in\Z} 
	  = B^{\T} \lp \dT{} + {B^{\Z} \over B^{\T} } \dz{} \rp \Qt e^{im\T -in\Z} 
          = i B^{\T} \lp m - n q \rp \Qt e^{im\T -in\Z} .
\end{equation}
Infinitely-many coordinate systems satisfy Eq.\ (\ref{StraightField}) 
(Hamada and Boozer coordinates being two
other commonly used choices) because one can alter both $\Z$ and $\T$ to satisfy the requirement. 
However, this 
system, sometimes called the PEST system because it was first introduced in the 
paper describing the PEST ideal-MHD code, has the advantage of having the simplest 
metric tensor due to the use of the axisymmetric toroidal angle.  A more
appropriate name is the symmetry coordinate system.

This coordinate system is often undesirable from a numerical viewpoint however.  The Jacobian of this
system is given by
\begin{equation}
\label{SFJac}
	\jac = {\dsdr q R^2 \over I}.
\end{equation}
The $R^2$ dependence means that the poloidal mesh of this system is not equally
distributed on the toroidal cross-section, especially at low-aspect ratio, which
could lead to poor numerical resolution.

%\begin{figure}
% \label{PegSF}
% \includegraphics[width=0.9\columnwidth]{PegSF.pdf} 
% \caption{Contours of constant $\T$ show that the PEST coordinate system leads to poor resolution on the outboard side at low aspect ratio.}
%\end{figure}

\subsection{Calculating Metric Elements Numerically}

The expressions in this section are sometimes known as {\em mapping relations}
because they represent a mapping from one (orthogonal) coordinate system to the
non-orthogonal coordinate system.  In most texts, the orthogonal coordinate
system used in the derivation shown is the standard Cartesian system, but for
toroidal systems, a cylindrical version is more useful.

The relation between the $R, \phi, Z$ cylindrical coordinate system that can be used to 
describe the toroidal plasma and the straight-field-line system is
\begin{equation}
\lp    
	\begin{array}{c}
		\grad R \\ \grad \phi  \\  \grad Z
	\end{array}	
\rp
=
\lp    
	\begin{array}{ccc}
		\dr{R} & \dT{R} & 0   \\
		   0   &    0   & -1  \\
		\dr{Z} & \dT{Z} & 0   
	\end{array}	
\rp
\lp    
	\begin{array}{c}
		\gr \\ \gt \\  \gz
	\end{array}	
\rp.
\end{equation}
Taking the inverse of this relation, one obtains
\begin{equation}
\lp    
	\begin{array}{c}
		\gr \\ \gt \\  \gz
	\end{array}	
\rp
=
\jaci R
\lp    
	\begin{array}{ccc}
		-\dT{Z} & 0  & \dT{R}   \\
		 \dr{Z} &  0 & -\dr{R}   \\
		  0     & {-\jac \over R}& 0   
	\end{array}	
\rp
\lp    
	\begin{array}{c}
		\grad R \\ \grad \phi  \\  \grad Z
	\end{array}	
\rp.
\end{equation}
This last relation is used to derive the desired expressions for the metric elements.
The metric elements may be written as
\begin{eqnarray}
&&	\grr = {1 \over R^2 \hat{\R}^2} \lb \lp \dT{R} \rp^2 + \lp \dT{Z} \rp^2 \rb, \nonumber \\
&&	\grt = {-1 \over R^2 \hat{\R}^2} \lb \lp \dT{R} \dr{R} \rp + \lp \dT{Z} \dr{Z} \rp \rb, \nonumber \\
&&	\gtt = {1 \over R^2 \hat{\R}^2} \lb \lp \dr{R} \rp^2 + \lp \dr{Z} \rp^2 \rb. \nonumber 
\end{eqnarray}

These relations are used to calculate the metric elements in an equilibrium code
where one knows the functions $R(\R,\T)$ and $Z(\R,\T)$.  

