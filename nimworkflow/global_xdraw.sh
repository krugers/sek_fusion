#!/bin/bash

#------------------------------------------------------------------
# global_funcs.sh
#  Various useful functions to use in nimrod management scripts
#  All require DRAWDIR to be defined
#  Functions provided
#    make_new_draw_file draw_file_name
#        Input at global scope: binfiles (an array of .bin files)
#	   Outputs:
#            A new draw file is create with name: $new_draw_file
#
#    get_drawfile_info bin_file_name
#        Determine drawfile corresponding to bin file
#	   Outputs: bin_prefix and draw_file
#        Example output:
#              bin_prefix=nimhist; draw_file=drawt.in
#            
#------------------------------------------------------------------

#------------------------------------------------------------------
# This stuff is to modify the draw file
#------------------------------------------------------------------
function make_new_draw_file {
      new_draw_file=$1

      cp $DRAWDIR/$draw_file ./$new_draw_file

      draw_bin_file=`grep \.bin $new_draw_file`

	# Header: Things before the bin file
	sed '1!G;h;$!d' $new_draw_file  \
		| sed -n "/$draw_bin_file"'/,$p' \
		| sed -n "/$draw_bin_file"'/!p'  \
		| sed '1!G;h;$!d' \
		> tmp1

	# Put files labeled 5 digits (or less) first
      if [ "${#binfiles[@]}" -gt 1 ]; then
            for file in ${binfiles[@]}; do
                  nlabel=`echo $file | sed "s/$bin_prefix//" | sed 's/\.bin//' `
                  numlabel=`expr $nlabel + 0`   	# Make sure it's a number
                  if [ "$numlabel" -lt 100000 ]; then
                        echo $file >> tmp1
                  fi
            done

            # Put labels with 6 digits next
            for file in ${binfiles[@]}; do
                  nlabel=`echo $file | sed "s/$bin_prefix//" | sed 's/\.bin//' `
                  numlabel=`expr $nlabel + 0`   	# Make sure it's a number
                  if [ "$numlabel" -ge 100000 ]; then
                        echo $file >> tmp1
                  fi
            done
      else
            echo ${binfiles[1]} >> tmp1
      fi

	# Now put rest of draw file (things after the bin file)
	sed -n "/$draw_bin_file"'/,$p' $new_draw_file | sed -n "/$draw_bin_file"'/!p' >> tmp1
	mv tmp1 $new_draw_file
}

#------------------------------------------------------------------
# Put all of the ugliness for figuring a draw file corresponding to a
# bin file in one spot.  
#------------------------------------------------------------------
function get_drawfile_info {
      case "$1"
      in
            # Draw file used depends on the prefix of the bin file
            xy_slicepf* | *_xy_slicepf*) 
                        bin_prefix=xy_slicepf
                        draw_file=drawxypf.in;;
            xy_slice* | *_xy_slice*) 
                        bin_prefix=xy_slice
                        draw_file=drawxy.in;;
             fc_slicepf* | *_fc_slicepf*) 
                        bin_prefix=fc_slicepf
                        draw_file=drawfcpf.in;;
            fc_slice* | *_fc_slice*) 
                        bin_prefix=fc_slice
                        draw_file=drawfc.in;;
            fc_negtmpf* | *_fc_negtmpf*) 
                        bin_prefix=fc_slicepf
                        draw_file=drawfcpf.in;;
            fc_negtm* | *_fc_negtm*) 
                        bin_prefix=fc_slice
                        draw_file=drawfc.in;;
            fcntpf* | *_fcntpf*) 
                        bin_prefix=fc_slicepf
                        draw_file=drawfcntpf.in;;
            fcnt* | *_fcnt*) 
                        bin_prefix=fc_slice
                        draw_file=drawfcnt.in;;
             fcpf* | *_fcpf*) 
                        bin_prefix=fc_slicepf
                        draw_file=drawfcpf.in;;
            fc* | *_fc*) 
                        bin_prefix=fc_slice
                        draw_file=drawfc.in;;
            xyntpfphi* | *_xyntpfphi*)   
                        bin_prefix=xyrzpfphi
                        draw_file=drawxypfphi.in;;
            xynt* | *_xynt*)   
                        bin_prefix=xynt
                        draw_file=drawnt.in;;
                        #draw_file=drawxy.in;;
            xyrzpfphi* | *_xyrzpfphi*)   
                        bin_prefix=xyrzpfphi
                        draw_file=drawxypfphi.in;;
             xyrzphi* | *_xyrzphi*)   
                        bin_prefix=xyrzphi
                        draw_file=drawxyphi.in;;
             xyrz* | *_xyrz*)   
                        bin_prefix=xyrz
                        draw_file=drawxy.in;;
            xypfnt* | *_xypfnt*) 
                        bin_prefix=xypfnt
                        draw_file=drawntpf.in;;
                        #draw_file=drawxypf.in;;
            xypfrz* | *_xypfrz*) 
                        bin_prefix=xypfrz
                        draw_file=drawxypf.in;;
            conntphi* | *_conntphi*)  
                        bin_prefix=conrzphi
                        draw_file=drawconrzphi.in ;;
             connt* | *_connt*)  
                        bin_prefix=connt
                        draw_file=drawconnt.in;;
                        #draw_file=contour.bin;;
            conrzphi* | *_conrzphi*)  
                        menu 'Do you want vector plots or contour plots?' "vector" "contour"
                        if [ "$global_option" = "vector" ];  then
                              bin_prefix=conrzphi
                              draw_file=drawvecphi.in
                        else
                              bin_prefix=conrzphi
                              draw_file=drawconrzphi.in
                        fi
                        ;;
             conrz* | *_conrz*)  
                        menu 'Do you want vector plots or contour plots?' "vector" "contour"
                        if [ "$global_option" = "vector" ];  then
                              bin_prefix=conrz
                              draw_file=drawvec.in
                        else
                              bin_prefix=conrz
                              draw_file=drawcon.in
                        fi
                        ;;
            nimfl* | *_nimfl*) 
                        bin_prefix=nimfl
                        draw_file=drawpss.in;;
            safefac* | *_safefac*) 
                        bin_prefix=safefac
                        draw_file=drawq.in;;
            rtout* | *_rtout*) 
                        bin_prefix=rtout
                        draw_file=drawrt.in;;
            energy* | *_energy*) 
                        bin_prefix=energy
                        draw_file=drawen.in;;
            logenergy* | *_logenergy*) 
                        bin_prefix=logenergy
                        draw_file=drawlogen.in;;
            logen* | *_logen*) 
                        bin_prefix=logen
                        draw_file=drawlogen.in;;
            lnsen* | *_lnsen*) 
                        bin_prefix=lnsen
                        draw_file=drawlnsen.in;;
            discharge* | *_discharge*) 
                        bin_prefix=discharge
                        draw_file=drawdis.in;;
            nimhist* | *_nimhist*) 
                        bin_prefix=nimhist
                        draw_file=drawt.in;;
            neutral* | *_neutral*)
                        bin_prefix=neutral
                        draw_file=drawneut.in;;
            2d_jac,*_2d_jac)
                  bin_prefix=2d_jac
                  draw_file=draw2d_jac.in;;
            2d_input* | *_2d_input*)
                  bin_prefix=2d_input
                  draw_file=draw2d_input.in;;
            2d* | *_2d*)
                  bin_prefix=2d
                  draw_file=draw2d.in;;
            contour* | *_contour*)
                  bin_prefix=contour
                  draw_file=drawcon.in;;
            divb* | *_divb*)
                  bin_prefix=divb
                  draw_file=drawdivb.in;;
            e_dot_j* | *_e_dot_j*)
                  bin_prefix=e_dot_j
                  draw_file=drawedj.in;;
            eig* | *_eig*)
                  bin_prefix=eig
                  draw_file=draweig.in;;
            eq_input* | *_eq_input*)
                  bin_prefix=eq_input
                  draw_file=draweq_input.in;;
            failure* | *_failure*)
                  bin_prefix=failure
                  draw_file=drawfailure.in;;
            flsurf* | *_flsurf*)
                  bin_prefix=flsurf
                  draw_file=drawfl.in;;
            grid* | *_grid*)
                  bin_prefix=grid
                  draw_file=drawgrid.in;;
            gs2d* | *_gs2d*)
                  bin_prefix=gs2d
                  draw_file=drawgs2d.in;;
            parallel_current* | *_parallel_current*)
                  bin_prefix=parallel_current
                  draw_file=drawjpar.in;;
            neo* | *_neo*)
                  bin_prefix=neo
                  draw_file=drawneo.in;;
            pack* | *_pack*)
                  bin_prefix=pack
                  draw_file=drawpack.in;;
            polflux* | *_polflux*)
                  bin_prefix=polflux
                  draw_file=drawpolfl.in;;
            polgrid* | *_polgrid*)
                  bin_prefix=polgrid
                  draw_file=drawpolgrid.in;;
            fluxgrid* | *_fluxgrid*)
                  bin_prefix=fluxgrid
                  draw_file=drawprof.in;;
            stability* | *_stability*)
                  bin_prefix=stability
                  draw_file=drawstab.in;;
            nimhist* | *_nimhist*)
                  bin_prefix=nimhist
                  draw_file=drawt.in;;
            xt_slice* | *_xt_slice*)
                  bin_prefix=xt_slice
                  draw_file=drawxteq.in;;
            yt_slice* | *_yt_slice*)
                  bin_prefix=yt_slice
                  draw_file=drawyt.in;;
            *)     
                  gfile=$1
		      if grep "$gfile" `/bin/ls $DRAWDIR/draw*.in`  > /dev/null 2>&1; then
                        draw_file=`grep "$gfile" "$DRAWDIR/draw*.in" | head -1 | cut -f1 -d:`
                        bin_prefix=${gfile%.bin}
                  else
                        echo "$1 is not a supported file type"
                        exit
			fi
                  ;;
      esac
}
