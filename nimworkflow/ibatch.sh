#!/bin/bash

#------------------------------------------------------------------
# Script: ibatch.sh		Authors: Scott Kruger
# Usage: ibatch.sh
# Description:  Interactive front end for the batch.sh script
#------------------------------------------------------------------
# Variables for users to setup.
#------------------------------------------------------------------
batch_setup="batch.sh"			# Assumes it's in your path
sp_exe="iplot.sh"			# Assumes it's in your path
nimrun_options="no yes"
hsiup_options="no yes"
#nimrun_exe="runnimrod.sh"
post_flag="no"
runnum=""
post=""
plot=""

#------------------------------------------------------------------
# Idiot check - don't run if nimrod.in isn't here
#------------------------------------------------------------------
if test ! -e "nimrod.in"; then
  echo "nimrod.in doesn't exist.  I think you are in the wrong directory"
fi
#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
#------------------------------------------------------------------
# This is redundant with batch.sh, but want to do for some checking
#------------------------------------------------------------------
host_info "batch_set"
if test -e $NIMVRS/share-par/$config_summary; then
	cp $NIMVRS/share-par/$config_summary ./${config_summary}-par
	cp $NIMVRS/share-ser/$config_summary ./${config_summary}-ser
elif test -e $NIMVRS/$config_summary; then
	cp $NIMVRS/$config_summary .
fi
#------------------------------------------------------------------
#  Determine whether linear or nonlinear
#------------------------------------------------------------------
get_namelist_val nonlinear
nonlinear=$var_value
if echo $nonlinear | grep -i f > /dev/null 2>&1; then nonlinear="f"; fi
#------------------------------------------------------------------
# START CODE EXECUTION HERE
#------------------------------------------------------------------
clear; echo; echo

i_options="interactive batch"
menu 'Do you want to run interactively or use batch system' $i_options
interflag="$global_option"

if test $interflag == "interactive"; then

      if test -e $config_summary || test -e ${config_summary}-par; then 
         menu 'Do you want to use mpirun to launch this job?' $nimrun_options
         if test "$global_option" == "yes"; then launch="-l"; fi
      else 
         launch=""
      fi 

	$batch_setup -i $launch

else

	echo ' '
	echo 'For the following menu choices, option 1 is your safest choice.'
	echo ' '

	menu 'Choose class of queue' $class_options
	if test -n "$global_option"; then class="-queue $global_option"; fi

	menu "Choose account to charge to $account_desc" $account_options
	if test -n "$global_option"; then account="-repo $global_option"; fi

	$batch_setup $class $account $runnum $plot 
#	$batch_setup $class $account $runnum $post $plot $linrod
#	$batch_setup $class $account $runnum $plot $linrod
fi

exit
