#!/bin/bash
#------------------------------------------------------------------
# Script: addrun.sh        		Author: Scott Kruger
# Usage: addrun.sh <old_dir> <new_dir>
# Description:  This script is for automatically adding directory 
#		  into svn.  Default files are defined
#------------------------------------------------------------------
#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
#------------------------------------------------------------------
# Set up some default files to add.  Easiest to base it on suffixes
#------------------------------------------------------------------
suffixes="in pre out sh html pbs ll summary"
specific_files="start_positions.dat"
#------------------------------------------------------------------
# Handle arguments
#------------------------------------------------------------------
while test -n "$1"; do
  case "$1" in
    -noedit)      noedit="yes"                              shift; ;;
    -help,--help) echo "Usage: $0 <directory> <files>";     exit  ;;
    *)            dirfiles=$@;                              break ;;
  esac
done

directory=( )
ndir=0
for dirfile in $dirfiles; do
      if [ -d $dirfile ]; then 
            let ndir=ndir+1
            directory[$ndir]=$dirfile
      fi
      if [ -e $dirfile ]; then files="$files $dirfile"; fi
done
#------------------------------------------------------------------
# Determine whether calling from parent directory or in directory
#------------------------------------------------------------------
if [ $ndir -eq 0 ]; then
      if [ ! -e "nimrod.in" ]; then
      	echo "nimrod.in does not exist.  Exiting"
      	exit
      else
            let ndir=ndir+1
            directory[$ndir]=`basename $PWD`
            startdir=$PWD
            cd ..
      fi
fi

#------------------------------------------------------------------
# directory loop
#------------------------------------------------------------------
current_dir=$PWD
for ((id=1; id <= $ndir ; id++)); do
	wdir=${directory[$id]}
	# Add directory Non-recursively
	if test ! -d $wdir/.svn; then
		echo "Adding directory to repo: " $wdir
      	svn add -N $wdir
	fi

      cd $wdir
      commitfiles=$specific_files
	added_files=""

	# .sh, .in., out, ...
      for ext in $suffixes; do
        extfiles=`ls *.$ext 2>/dev/null`
	  if [ "$ext" = "in" ]; then extfiles=`ls *.$ext | grep -v draw 2>/dev/null`; fi
        commitfiles="$commitfiles $extfiles"
      done

      if test -n "$commitfiles"; then
        for cfile in $commitfiles; do
	     if svn status -N | grep $cfile | grep ^\?  > /dev/null 2>&1; then
              svn add $cfile
		  added_files="$added_files $cfile"
	     fi
        done
      fi
	echo

      if test -n "$added_files"; then
	  echo "Size of files to commit:"
        du -sh $added_files
      fi
    cd $current_dir
done

echo "Need to type 'svn commit' now to make sure changes are committed"
