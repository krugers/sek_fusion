#!/bin/sh
#----------------------------------------------------------------------
# Script: debug_script		Author: Eric Held (eheld@cc.usu.edu)
# Attached is a script for obtaining interactive nodes on seaborg.  To
# use it type:
# 			llsubmit debug_script
# What happens is you get to work interactively on however many nodes and
# processors you request. I typically submitted to the debug queue,
# waited about 5 minutes and then had an X window to either run an
# executable directly or fire up totalview. The catch is that in the
# debug queue you only have 30 minutes. If you really need a node for
# some serious debugging, you could in principle submit to the premium
# queue and work for the full 8 hour limit. Unfortunately, because of
# the ridiculous charge factors on seaborg, this chews up some serious CPU
# time. The debug option seems the best in order to get processors
# quickly and have some time to do something.
# 
# The other option when interactive nodes are hard to come by is to use
# the retry extension. For example,
# 	totalview poe -a ./nimrod/ -nodes 1 -procs 16 -retry 10 -retrycount 1000
# will continue to submit the interactive job every 10 seconds for a total
# of 1000 times This way as soon as an interactive node is freed up you
# stand a good chance of getting it.
#----------------------------------------------------------------------
#@ job_name = totalview
#@ output = $(host).$(jobid).out
#@ error = $(host).$(jobid).err
#@ job_type = parallel
#@ wall_clock_limit= 08:00:00
#@ notification = never
##@ environment = MP_INFOLEVEL=2 ; MP_LABELIO=yes
#@ environment = COPY_ALL
#@ network.MPI = csss,not_shared,us
#@ node            = 1
#@ tasks_per_node  = 16
#@ class = debug
##@ class = premium
#@ queue


hostname

date

xterm -geometry 99x34 -bg brown -fg yellow -sb -sl 600 -ls -fn 9x15

date
