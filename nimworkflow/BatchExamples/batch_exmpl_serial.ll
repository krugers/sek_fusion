#@ shell = /usr/bin/csh
#@ environment = COPY_ALL
#@ job_name        = MDS+
#@ output          = $(jobid).out
#@ error           = $(jobid).err
#@ account_no      = mp21
#@ job_type        = serial
#@ wall_clock_limit= 8:00:00
#@ notification    = never
#@ class           = regular
#@ queue

#
for file in `ls -r dump.*`
do
      argument=`echo $file | sed "s/dump\.//"`
	echo $argument
	~/nimroot/Cooker/nimplot/nimds $argument > nimds.out$argument
done
