#QSUB -s /bin/csh			#Specify C Shell
#QSUB -r NIM.VAC1 			#Job name
#QSUB -ro -o batch.log -eo  		#Write NQS error & output to single file.
#QSUB -l mpp_t=15000			#Maximum residency time
#QSUB -l p_mpp_t=15000 			#Maximum residency time for a process
#QSUB -l mpp_p=27			#Maximum PEs Needed (Required).
#QSUB -ln 6				#Niceness level
#
echo Beginning of Execution `date`
set echo
ja                                      #Turn on Job Accounting
mpprun -n 27 $NIMSRC/nimrod/nimrod 
ja -s                                   #Print Job Accounting Summary
#./newrun
exit
