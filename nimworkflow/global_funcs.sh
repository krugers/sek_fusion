#!/bin/bash
#------------------------------------------------------------------
#  Yes, you can think of almost everything as a function, 
#  but this may upset your wife.
#		-- Larry Wall
#------------------------------------------------------------------
# global_funcs.sh
#  Various useful functions to use in nimrod management scripts
#  Functions for modifying files
#	 replace_line 
#  Functions for using nimrod namelists:
#	 get_namelist_val 
#	 get_modes 
#	 modify_namelist 
#	 fix_comment_line 
#	 get_runlabel
#  Functions for file management:
#	 link_file
#	 copy_file
#	 make_version_bindir
#  Other functions
#	 host_info
#	 
#  To do:
#    In the future, I should make replace_line, get_namelist_val,
#    modify_namelist stand-alone python functions.  These are the most
#    common functions and yet bash isn't up to the task here.
#------------------------------------------------------------------
#topscriptdir=`dirname $0`
source global_library.sh
#------------------------------------------------------------------
# Provenance notes:
#   In batch.sh, I create file: run_summary_file="nimrun.summary"
#   runnimrod.sh adds to this file as well
# config_summary file needs to be consistent with configure.ac file
# config_summary file copied over if found so we know the build.
# status_file is used in runnimrod.sh and nimdex
#------------------------------------------------------------------
run_summary_file="nimrun.summary" 	# Name of run-management file 
config_summary="config.summary"
tmp_root_name=`dirname $PWD`
index_file=../`basename $tmp_root_name`_notes
if test -n "$SCRATCH"; then
	status_file=$SCRATCH/run_status
else
	status_file=run_status
fi
#status_file=../`basename $PWD | sed "s/[0-9].*//g"`_status

svnroot="https://nimrodteam.org/svn"

#------------------------------------------------------------------
# Global vars
#------------------------------------------------------------------
LN_S='ln -s'

nimrod_executables="fluxgrid/fluxgrid \
	nimset/nimset nimset/nimreset nimset/nimbase nimset/stitch \
	nimrod/nimrod nimrod/linrod \
	nimfl/nimfl \
	nimplot/nimplot nimplot/xlog nimplot/svalue \
	nimplot/nimtec nimplot/nimhdf nimplot/nimDX nimplot/nimdraw \
	dump_convert/dump_convert dump_convert/dump_conv30to31"

input_file="nimrod.in"

#------------------------------------------------------------------
# Replaces a line denoted by keyword with a newline
#------------------------------------------------------------------
function replace_line ()
{
	file="$1"
	keyword="$2"
	new_line="$3"

	# Stuff above keyword line
	# Something like this might work better
	# head -n`grep -n EDITOR .cshrc | cut -f1 -d:` .cshrc
	sed '1!G;h;$!d' $file  \
		| sed -n "/$keyword"'/,$p' \
		| sed -n "/$keyword"'/!p'  \
		| sed '1!G;h;$!d' \
		> tmp_replace_line
	
	echo $new_line >> tmp_replace_line
	
	# Stuff below keyword line
	sed -n "/$keyword"'/,$p' $file \
		| sed -n "/$keyword"'/!p' \
		>> tmp_replace_line

	mv tmp_replace_line $file
}

#------------------------------------------------------------------
# Get the namelist value correcsponding to the var input
# grepped_var commands by line:
#	Remove tabs
#	Remove namelist terminating char: \
#	Remove namelist comments  !*
#	Remove commas
#	Remove trailing white space
#	Next 2 lines: Remove spaces on either side of = sign
# This is a function that I should just make into python executable
#------------------------------------------------------------------
# Takes argument.  Sets $var_value
function get_namelist_val ()
{
	  param=$1
	  if test ${#@} -eq 2; then
		  nlfile=$2
	  else
		  nlfile=$input_file
	  fi
        grepped_var=`grep -v ^! $nlfile | \
                         tr '\011' ' ' | \
                         sed 's/\\\//g' | \
                         sed 's/!.*$//g' | \
                         sed "s/,//g" | \
                         sed "s/\ *$//" | \
                         sed 's/\ *=/=/g' |
                         sed 's/=\ */=/g' |
                         grep \ $param= `

	  # Strip beginning white space
        grepped_var=`echo $grepped_var | sed "s/^\ *//"`

        #Handle character namelists which may contain an = sign
        var_value=`echo $grepped_var | sed s/\"$//g | sed s/^.*\"//g`
        if [ "$var_value" = "$grepped_var" ];  then
        	var_value=`echo $grepped_var | sed s/\'$//g | sed s/^.*\'//g `
        fi

        #Other namelist variables
        if [ "$var_value" = "$grepped_var" ];  then
         var_value=`echo $grepped_var | sed 's/^.*=//' | sed 's/\ //g'`
        fi
	  return 
}
#------------------------------------------------------------------
#
#------------------------------------------------------------------
function get_modes ()
{
	get_namelist_val nonlinear
	nonlinear=$var_value
	if echo $nonlinear | grep -i t > /dev/null 2>&1 
	then
		nonlinear="t"
		get_namelist_val lphi
		lphi=$var_value
	      nmodes=`echo "2^$lphi" | bc -l`
	      nmodes=`expr $nmodes / 3 + 1`
		mode_start=0
		mode_end=`expr $nmodes - 1`
	else
		nonlinear="f"
		get_namelist_val lin_nmodes
		lin_nmodes=$var_value
	      nmodes=$lin_nmodes
		get_namelist_val lin_nmax
		lin_nmax=$var_value
		mode_start=`expr $lin_nmax - $lin_nmodes + 1`
		mode_end=`expr $nmodes`
	fi

	for ((ik=0; ik < $nmodes ; ik++))  # Double parentheses
	do
	  keff[$ik]=`expr $mode_start + $ik`
	done
	return
}
#------------------------------------------------------------------
# Change the value I want.  Use python script to be more robust
#------------------------------------------------------------------
function modify_namelist
{	
    param=$1; new_value=$2
    modnl.py $input_file $param $new_value
}
#------------------------------------------------------------------
# Change comment to current directory and add a date stamp.
# Comments are denoted by ! as first character and text. e.g. !hbd1002
#------------------------------------------------------------------
function fix_comment_line {	
	job_name=$1
	orig_job=$2
	tab_char=`echo X | tr 'X' '\011'` 	# Crude yet effective
	date="$tab_char$tab_char`date`"
	new_comment_line="!$job_name (from $orig_job) $date"
	if test -f 'nimrod.in'; then
	 comment_line=`grep ^\![a-zA-Z1-9] nimrod.in | head -1`
	 if [ -n "$comment_line" ]; then
  	  sed "s/$comment_line/$new_comment_line/" 'nimrod.in' > tmpnimrod.in
          mv tmpnimrod.in nimrod.in
	 fi
	fi
}
#------------------------------------------------------------------
# Return run_label and run_line: run_label based on pwd or label in file
#------------------------------------------------------------------
function get_run_label {
	if test -z "$run_label"; then
		if grep ^!  < "$input_file" | sed "s/!//" | grep ^[a-zA-Z1-9] > /dev/null 2>&1 
		then
			run_line=`grep ^! $input_file | sed "s/!//" | grep ^[a-zA-Z1-9]`
			run_label=`echo $run_line | cut -f1 -d" "`
		else
			run_label=`basename $PWD`
			run_line=$run_label
		fi
	fi
}

#------------------------------------------------------------------
# append comment lines
#------------------------------------------------------------------
function append_comment_lines {	
      append_file=$1
	comment_lines=`grep ^! $input_file |  grep -v "$run_line" | sed "s/!//" `
	#comment_lines=`grep ^! $input_file |  grep -v "$run_label_line" | sed "s/!//"| fmt`
	# grab lines below run_label
	sed -n "/^!$run_label"'/,$p' $input_file | sed -n "/$run_label"'/!p' | sed 's/^!//g' \
		>> $append_file
}
#------------------------------------------------------------------
# Safe linking
#------------------------------------------------------------------
function link_file
{
	existingfile=`basename $1`
	if [ -e $existingfile ]; then rm $existingfile; fi
	if [ -e $1 ]; then $LN_S $1 .;  fi
}

#------------------------------------------------------------------
# Safe copying
#------------------------------------------------------------------
function copy_file
{
	if test -n $2; then
		if [ -e $1 ]; then cp $1 $2;  fi
	else
		if [ -e $1 ]; then cp $1 .;  fi
	fi
}

#------------------------------------------------------------------
# Create $NIMSRC/bin if it doesn't exist and set the links
# Call with top level root dir (e.g., make_version_bindir $NIMSRC)
#------------------------------------------------------------------
function make_version_bindir
{	
	cdir=$PWD
	root_dir=$1
	cd $root_dir
	if [ ! -d bin ]; then mkdir bin; fi
	cd bin
	if [ -d ../Source/nimrod ]; then
		lnroot="../Source"
	elif [ -d ../nimrod ]; then
		lnroot=".."
	else
		echo "Abort!!!"; exit
	fi

	for executable in $nimrod_executables; do
		link_file $lnroot/$executable .
	done

	# Copy over $config_summary  file
	if [ -e ../$config_summary ]; then
		link_file ../$config_summary .
	fi

	cd $cdir
}
#------------------------------------------------------------------
#  Set location and hostname info that's more useful
#  Variables:
#   	hostnm
#   	location
#   	bindirname        (for binaries svn repo)
#------------------------------------------------------------------
function host_info
{	
	if test ${#@} -ge 1; then
		batchflag="$1"
	fi
	hostnm=`hostname`
	if [ -f /usr/ucb/hostname ]; then hostnm=`/usr/ucb/hostname`; fi
	machinetype=`uname`

	case $hostnm in
	  # davinci
	  davinci*)      
		hostnm="davinci"; location="nersc"; bindirname="seaborg"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_aix_nersc.in"
	            source $topscriptdir/BatchConfig/batch_aix_nersc.sh 
		fi
		;;
	  # seaborg
	  s00*)      
		hostnm="seaborg"; location="nersc"; bindirname="seaborg"
		if test -n "$batchflag"; then 
            	    batch_template="nimbatch_aix_nersc.in"
	            source $topscriptdir/BatchConfig/batch_aix_nersc.sh 
		fi
		;;
	  # Franklin
	  nid0*)  
		hostnm="franklin"; location="nersc"; bindirname="franklin"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_pbs_franklin.in"
	            source $topscriptdir/BatchConfig/batch_franklin.sh
		fi
		;;
	  # jacquard
	  jacin*)    
		hostnm="jacquard"; location="nersc"; bindirname="jacquard"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_pbs_nersc.in"
	            source $topscriptdir/BatchConfig/batch_pbs_nersc.sh 
		fi
		;;
	  # bassi
	  b0*)       
		hostnm="bassi"; location="nersc"; bindirname="bassi"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_aix_nersc.in"
	            source $topscriptdir/BatchConfig/batch_aix_bassi.sh 
		fi
		;;
	  # sunfire
	  sunfire*.pppl.gov)  
		hostnm="sunfile"; location="pppl"; bindirname="sunfire"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_pbs_generic.in"
	            source $topscriptdir/BatchConfig/batch_sgi_pppl.sh 
		fi
		;;
	  # jaguar
	  jaguar*)  
		hostnm="jaguar"; location="ornl"; bindirname="jaguar"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_pbs_nccs.in"
	            source $topscriptdir/BatchConfig/batch_pbs_nccs.sh
		fi
		;;
	  # oxygen
	  oxygen*)  
		hostnm="oxygen"; location="Tech-X"; bindirname="oxygen"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_pbs_oxygen.in"
	            source $topscriptdir/BatchConfig/batch_pbs_oxygen.sh
		fi
		;;
	  # storage2
	  storage2*)  
		hostnm="storage2"; location="Tech-X"; bindirname="storage2"
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_pbs_storage2.in"
	            source $topscriptdir/BatchConfig/batch_pbs_storage2.sh
		fi
		;;
	  # Generic bewoulf cluster
	  *)
		hostnm="generic"; location="generic"; bindirname="linux-lf95"
		case $mashinetype in
			Linux)   bindirname="linux-lf95";;	# Need other compilers
			Darwin)  bindirname="darwin-xlf";;	# Need PPC check
		esac
		if test -n "$batchflag"; then 
            	    batch_template="BatchTemplates/nimbatch_pbs_generic.in"
	            source $topscriptdir/BatchConfig/batch_pbs_generic.sh
		fi
		;;
		#SEK: NEED TO FIX THIS
	esac

}

