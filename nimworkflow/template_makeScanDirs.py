#!/usr/bin/env python
#------------------------------------------------------------------
# EDIT THIS.
#  If you want to change the defaults.  Then edit:
#    $NIMROOT/bin/template_makeScanDirs.py
#------------------------------------------------------------------

# Count this beginning with 0
varOrder=['kpll_set','dvac_set','spatial_set']

Vars={}
Vars['kpll_set']={}
Vars['kpll_set']['k_pll']=["1.2e6","1.2e7","1.2e9"]
Vars['dvac_set']={}
Vars['dvac_set']['dvac']=["1.E5","1.E6","1.E7"]
Vars['spatial_set']={}
Vars['spatial_set']['mx']=["10","15","30","40"]
Vars['spatial_set']['my']=["10","15","30","40"]
Vars['temporal_set']={}
Vars['temporal_set']['dtm']=["1.E-8","1.E-7","1.E-6"]
Vars['temporal_set']['ndump']=["1000","100","10"]  # want fixed tau_A dump

#------------------------------------------------------------------
#  Accepted forms of run label: run003a OR run003 OR 003a
#  If it ends with letter, then we can increment that letter
#   for the first set specified in varOrder (always first)
#------------------------------------------------------------------
setIncrLetter=False    # Turned off by default
#setIncrLetter=True    # Increment the last letter if the first set

#------------------------------------------------------------------
# This is what allows one to import this file without problem,
# but execute it if invoked directly
#------------------------------------------------------------------
import sys, os, shutil, string
nimPyPath=os.getenv('NIMROOT')+"/bin"
sys.path.append(nimPyPath)
from ScanDirs import *              # This is where the mojo occurs
if __name__ == '__main__': main(Vars,varOrder,setIncrLetter)
