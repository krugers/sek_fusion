#!/bin/bash
#------------------------------------------------------------------
# Script: pxd         		Author: Scott Kruger
# Usage: pxd <file>
# Description:  This script is for automatically viewing 
#		bin files renamed by the setup.plot/batchplot scripts
#		and for any file whose draw file is in the NIMSRC/draw
#		directory.
#           Version 2: Merging xd, mxd, and pxd functionality into
#               one script.  mxd is now done via: "pxd energy0*.bin"
#               This syntax is simpler, more intuitive, and results
#               in more compact code.
#------------------------------------------------------------------
#------------------------------------------------------------------
# Handle arguments
#------------------------------------------------------------------
usage_line="Usage: `basename $0` [--help] [-norm] [-edit] [label] [binfiles]"
if test ${#@} -eq 0; then
      echo $usage_line; exit
else
      while test -n "$1"; do
        case "$1" in
          -norm) 
                       norm_file="yes"; shift ;;
          -edit) 
                       copy_file="yes"
                       edit_file="yes";   shift ;;
          -cp) 
                       copy_file="yes"
                       norm="yes";   shift ;;
          -help) 
                       echo $usage_line;  exit ;;
          --help) 
                       echo $usage_line;  exit ;;
          *) 
                       filelabels=$@;     break ;;
        esac
      done
fi

nfile=0; binfiles=( )
for filelabel in $filelabels; do
      if test "${filelabel##*.}" == "bin"; then 
            let nfile=nfile+1
            binfiles[$nfile]=$filelabel
      else
		# Just the label was supplied
            if test ${#@} -gt 1; then
                  echo $usage_line
                  exit
            else
			xdoption=$filelabel
            fi
      fi
done

#------------------------------------------------------------------
# Print help messages
#------------------------------------------------------------------
function print_useful_info {
	# No point in telling user file you are creating a file
	# if you are going to remove it
	if test -z "$remove_file"; then
		echo "Creating new draw file: draw$xdoption.in"
		echo
		if test -z "$edit_file"; then
			echo "You may edit it after viewing"
			echo "or rerun `basename $0` with the -edit option"
		fi
	fi
}
#------------------------------------------------------------------
# Global nimrod and xdraw functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
	source $topscriptdir/global_xdraw.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi

#------------------------------------------------------------------
# Traditional pxd + mxd -- create draw file based on file name(s)
#------------------------------------------------------------------
if test $nfile -gt 0; then
	get_drawfile_info ${binfiles[1]}
	nlabel=`echo ${binfiles[1]} | sed "s/$bin_prefix//" | sed 's/\.bin//' `
     	#xdoption=$bin_prefix$nlabel
     	xdoption=$bin_prefix
	print_useful_info 
      make_new_draw_file draw$xdoption.in
#------------------------------------------------------------------
# Tradition xd stuff -- direct linking in based on a label
#------------------------------------------------------------------
else
	new_draw_file=draw$xdoption.in
     	cp -f $DRAWDIR/$new_draw_file .
fi
#------------------------------------------------------------------
# Edit if requestied
#------------------------------------------------------------------
if test -n "$edit_file"; then
	$EDITOR $new_draw_file
fi
#------------------------------------------------------------------
# Run xdraw
#------------------------------------------------------------------
xdraw $xdoption
#------------------------------------------------------------------
# Remove draw file
#------------------------------------------------------------------
if test ! "$norm" == "yes"; then 
	rm $new_draw_file
fi
