#!/bin/bash
#------------------------------------------------------------------
# Script: manydump		Author: Scott Kruger
# Usage: manydump
# Description:  This script is for automatically running programs
#	to analyze dump file, such as nimplot, nimtec, ...
#	For nimplot, it uses the np script.  See that as well.
#	It acts on each file in the argument list.  So using
#		manydump dump.*
#	will analyze all of the dump files
#------------------------------------------------------------------
#
nimplot_exe=$NIMBLD/nimplot/nimplot
usage_line="Usage: `basename $0` <file(s)>"
#------------------------------------------------------------------
#  Check usage
#------------------------------------------------------------------
if [ "$#" -lt 1 ]; then echo $usage_line; exit; fi

#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi

#------------------------------------------------------------------
# Rename the file according to type
#------------------------------------------------------------------
function rename_file ()
{
	label=`echo $1 | sed "s/\.npin//" | sed "s/^\.//"`
	step_number=`echo $2 | sed "s/dump\.//"`
	case $label
	in
	      xypf*)  old_name="xy_slicepf.bin" ;;
	      xy*)  old_name="xy_slice.bin"
			if [ ! -e $old_name ]; then
				old_name="xy_slice.$step_number.bin"
			fi
			;;
	      con*)   old_name="contour.bin" ;;
	esac
	new_name="$label"_"$step_number.bin"
	
	echo "Creating " $new_name
	mv $old_name $new_name
}

#------------------------------------------------------------------
# START CODE EXECUTION HERE
#------------------------------------------------------------------
for dump_file in $@
do
	if [ ! -e $dump_file ]; then
		echo $dump_file" does not exist"
		exit
	fi
done
for input_file in `ls .*.npin`
do
	label=`echo $input_file | sed "s/\.npin//" | sed "s/^\.//"`
	case $label
	in
	      con*)
		      if test $# -gt 1; then
				echo "Do you wish to put all of the dump files into a single bin file?"
				read_yesno_response; build_yesno=$yn_response
		      else
			    build_yesno="n"
		      fi
			if test "$build_yesno" == "y"; then
				cp $input_file npin
				i=0
				for dump_file in $@
				do
					let i=i+1
					if test "$i" -eq "$#"; then
					  sed "s/dump.xxxxx/`echo $dump_file`/g" npin > npintmp
					else
					  sed "s/dump.xxxxx/`echo $dump_file`\n\0/g" npin > npintmp
					fi
					mv npintmp npin
				done
				#$nimplot_exe noplot < npin >> nimplot_exe.log
				exit
				touch contour.bin
				rename_file $input_file $dump_file
			else
				for dump_file in $@
				do
					echo "Processing " $dump_file
					sed "s/dump.xxxxx/`echo $dump_file`/g" $input_file > npin
					$nimplot_exe noplot < npin >> nimplot_exe.log
					rename_file $input_file $dump_file
				done
			fi
		;;
	      *)  
			for dump_file in $@
			do
				echo "Processing " $dump_file
				sed "s/dump.xxxxx/`echo $dump_file`/g" $input_file > npin
				$nimplot_exe noplot < npin >> nimplot_exe.log
				rename_file $input_file $dump_file
			done
			;;
	esac
done

rm npin
