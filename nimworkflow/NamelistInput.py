"""
 NamelistInput.py: A module for putting parsed namelists into an object
"""

import read_namelist, shutil

# This puts the namelist values from an input file and inserts the values
# into a CodeInputClass
class NamelistInput:
      type="namelist"

      def __init__(self,input_files=[""],doc_init=1):
          import os

          #Save some useful info
          self.files=input_files

          # Parse input file and place info into class structure
          if input_files[0] !="": self._file_input(input_files)


      def _file_input(self,input_files):
          """Parse a code input file and add the data to CodeInput
          class structure (including the dictionary)."""

          def create_masterindex(nldict):
           """ create_masterindex:  
               This goes through and creates a global index.  The
               key is given by the variable name.  If the same
               name is in multiple namelist files, then there
               will be some overwrites
           """
           for file in nldict.keys():
              for nl_name in nldict[file].keys():
                 if nl_name=="header" or nl_name=="tailer": continue
                 for key in nldict[file][nl_name]['variables'].keys():
                     self.variable[key]=nldict[file][nl_name]['variables'][key]
                     self.varinfo[key]={}
                     self.varinfo[key]=nldict[file][nl_name]['varinfo'][key]

          #Put namelist info into dictionary structure
          self.variable={}
          self.varinfo={}
          self.filedata=read_namelist.process_files(input_files)
          create_masterindex(self.filedata)


      def save(self):
          """ Save the input file
          """
          self.saveas(self.files)

      def saveas(self,output_files):
          """ Modify the input file(s) with the current values of the input variables.
              Note that if you 
          """
          import string,re
          if type(output_files)==type(''): output_files=[output_files]
          varval=re.compile(read_namelist.varval_split_pat)

          if len(output_files) > len(self.files):
                print "I cannot write to more input files then what was read in."
                return

          for rf, wf in [(rf,wf) for rf in self.files for wf in output_files]:
              #print rf, wf
              if rf == wf:
                wf=wf+"_tmp"
                temp_file=True
              read_file  = open( rf, 'r' ); write_file = open( wf, 'w' )
              for line in read_file:
                  # Ignore blank and commented lines
                  if len(varval.split(line))>1 and varval.split(line)[0].strip() != '': 
                    #Variable and value
                    var,val=varval.split(line)[0].strip(),varval.split(line)[1].strip()
                    newval=self.variable[var]
                    if type(newval) != type(''): newval=str(newval)
                    #print '                ====>',var,val,newval
                    line=string.join(line.split(val),newval)

                  write_file.write(line)
              read_file.close()
              write_file.close()
              if temp_file: shutil.move(wf,rf)

      def print_file_vars(self):
          """ walk through the filedata dictionary and print the
          variables.  I can tell it is a variable because it is the
          bottom of the tree"""
          nldict=self.filedata
          for file in nldict.keys():
             print 'file = ', file
             for nl_name in nldict[file].keys():
                print '   nl_name = ',    nl_name
                if nl_name=="header" or nl_name=="tailer": 
                    print "     nl_name ", nl_name, " ||> ", nldict[file][nl_name]
                else:
                  for key in nldict[file][nl_name]['variables'].keys():
                    value=nldict[file][nl_name]['variables'][key]
                    comment=nldict[file][nl_name]['varinfo'][key]['comment']
                    if comment == "":
                      print "     var, value |=> ", key, " = ", value
                    else:
                      print "     var, value |=> ", key, " = ", value, "         |", comment


      def change_var(self,var,value):
          """ given a variable and it's value, change the values
              stored in the dictionary
          """
          self.variable[var]=value

#      def set2default(self,var):
#          """ This sets the current value with the default value as
#              defined in the documentation file.
#          """
#          if not self.doc_init:
#              print "Documentation data is not initialized"
#              return
#          indices=self.varinfo[var]["doc_index"]
#          ilength=len(indices)-1
#          group=self.doc
#          for gindex in indices[:ilength]:
#            group=group.group[gindex]
#          found_var=group.variable[indices[ilength]]
#          if hasattr(found_var,'default_value'):
#              self.variable[var]=found_var.default_value
#          else:
#              print 'default value does not exist'
#          return
#
#      def set2default_all(self):
#          """ This changes the value of the variables in the
#          original data file with the value stored in the 
#          documentation file.
#          """
#          for var in self.variable.keys():
#              self.set2default(var)

      def set2original(self,var):
          """ Set the current value with original value that was read in.
          """
          indices=self.varinfo[var]["dict_index"]
          group=self.filedata
          for gindex in indices:
            group=group[gindex]
          self.variable[var]=group[var][0]

      def set2original_all(self,var):
          """ Changes all of the current values to the original values
              that were read in.
          """
          for var in self.variable.keys():
              self.set2original(var)
