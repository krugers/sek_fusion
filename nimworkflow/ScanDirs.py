#!/usr/bin/env python
import sys, os, shutil, string
import optparse
from NamelistInput import *

class dirsClass:
      def __init__(self,start_dir,odir,incrLetter):
            self.start_dir=start_dir
            self.prev_dir=start_dir
            self.root_dir=odir
            self.new_dir=""
            # Things for allowing increment of letter
            self.incrLetterNow=False
            self.incrLetter=incrLetter
            self.resetDir=start_dir
            self.setDir=False   # for keeping track of init of resetDir

class summaryLists:
      def __init__(self):
            self.summaryList=[]
            self.varsList=[]
            self.dirsList=[]

#------------------------------------------------------------------
# Get the new dirname
#  For below, if prev_dir="run003a", then
#    run_number='003'; firstString="run"; lastString="a"
#------------------------------------------------------------------
def get_new_dirname(myD,offset=0):
      # Get the run number
      run_number=myD.prev_dir.lstrip(string.letters).rstrip(string.letters)
      firstString=myD.prev_dir.split(run_number)[0]
      lastString =myD.prev_dir.split(run_number)[1]

      # If it is time to increment letter, then do it
      #  then increment that letter.  Otherwise, increment number
      if myD.incrLetterNow:
         # If blank, set it up such that incremented letter is "a"
         if lastString != "":
           lastLetter=lastString[-1:]
           lIndex=string.letters.index(lastLetter)
         else:
           lIndex=-1
         # If we've hit "Z"; then move to "aa"
         if lIndex != len(string.letters)-1:
               incrLetters=string.letters[lIndex+1]
         else:
               incrLetters="aa"
         lastString=lastString[:-1]+incrLetters
         
      # Normal increment: Just increment the run_number
      else:
         num_zeros=len(run_number)-len(str(int(run_number)))
         new_number=int(run_number) + 1
         if int(new_number) == 10: num_zeros=num_zeros-1
         if int(new_number) == 100: num_zeros=num_zeros-1
         run_number='0' * num_zeros + str(new_number)

      # Now that I've incremented things, put the run label back
      # together
      new_label=firstString + run_number + lastString

      # If we incremented a letter, we need to keep track of this
      if myD.incrLetterNow: 
            myD.resetDir=new_label
            myD.incrLetterNow=False
      # The first step is always tricky
      if not myD.setDir: 
            myD.resetDir=new_label
            myD.setDir=True

      return new_label

#------------------------------------------------------------------
# Make the new directory, enter the directory, and make the new 
# namelist
#if not os.path.basename(prev_dir) == "": prev_dir=os.path.basename(prev_dir)
#------------------------------------------------------------------
def make_newdir(old_dir,new_dir):
      cmd="mknewrun.sh -nointeract -none " + old_dir +" "+ new_dir
      os.system(cmd)
      return

#------------------------------------------------------------------
# Return a list of the variables that we will modify
#------------------------------------------------------------------
def getVarList(vars,order):
      vList=[]
      for set in order:
         for varKey in vars[set].keys():
               vList.append(varKey)
      return vList

#------------------------------------------------------------------
# A recursive function for traversing all of the permutations
# vars: the dictionary of variable and values grouped by sets
# order: a list that specifies the order of sets to apply
# currentLevel: number that corresponds to which
#  index in the list "order" that we are on
# currentValVars: keeps track of all of the set of Var-Val pairs
#  that we want to apply to modify the namelist
#------------------------------------------------------------------
def varLoop(vars,order,currentLevel,currentValVars,myDirs,mySum):
      keyLevel=order[currentLevel]          # This is the order we are
      # Check to see if we need to increment letter
      if currentLevel==1 and myDirs.incrLetter:
         if not myDirs.resetDir == myDirs.prev_dir:
             myDirs.incrLetterNow=True
             myDirs.prev_dir=myDirs.resetDir

      # Need to find the number of parameters to loop over
      # Find by looking at just the first variable length
      firstkey=vars[keyLevel].keys()[0]
      numVals=len(vars[keyLevel][firstkey])

      # Loop over each value at this level
      for ival in range(numVals):
            for var in vars[keyLevel].keys():
                  currentValVars[var]=vars[keyLevel][var][ival]
            if currentLevel != len(order)-1:
                  nextLevel=currentLevel+1
                  varLoop(vars,order,nextLevel,currentValVars,myDirs,mySum)
            else:
                  setNumber=order.index(keyLevel)
                  new_dir=get_new_dirname(myDirs)
                  mySum.dirsList.append(new_dir)
                  make_newdir(myDirs.prev_dir,new_dir)
                  myDirs.prev_dir=new_dir # Update prev_dir dir
                  summaryLine=new_dir +"=> " + str(currentValVars)
                  mySum.summaryList.append(summaryLine)
                  os.chdir(new_dir)
                  input=NamelistInput(["nimrod.in"])
                  for var in currentValVars.keys():
                        val=currentValVars[var]
                        input.change_var(var,val)
                  input.save()
                  os.chdir(myDirs.root_dir)


#------------------------------------------------------------------
# After all is said and done, summarize the directories created,
#  the variables modified, etc.
#------------------------------------------------------------------
def summarizeRuns(summaryFile,mySum):
      # This is the order we are:
      sumFile=open(summaryFile,"w")
      for idir in range(len(mySum.summaryList)):
            sumFile.write(mySum.summaryList[idir]+"\n")
      sumFile.close()
      varStr=""; dirStr=""
      for var in mySum.varsList:
            varStr=varStr + " " + var
      for dir in mySum.dirsList:
            dirStr=dirStr + " " + dir
      print "Finished generating the directories."
      print "To see a summary of the changes, see ", summaryFile
      print " or type: "
      print " summarize -vars '" +varStr +"'"+ dirStr
      print 
      print "Now type: many_nimset <dirs>"
      print "And then type: imaster.sh"


#------------------------------------------------------------------
# Main part of driving the routine.
#------------------------------------------------------------------
def main(Vars,varOrder,IncrLetter):
    parser = optparse.OptionParser()
    parser.add_option('-l', '--label', dest='label', help='A label for the runs', default='')
    options, args = parser.parse_args()

    # Get the program name
    iarg=1
    if sys.argv[0].rfind("python")<0: iarg=0
    program_name=os.path.basename(sys.argv[iarg])

    # Get the base directory
    if len(args) != 1:
          print "Usage: "+ program_name+ " Base_directory"
          sys.exit()
    else:
          start_dir=args[0].rstrip("/")

    # See if we have a label
    if options.label == "":
          summary_file="ScanSummary.txt"
    else:
          summary_file=options.label+"_ScanSummary.txt"
 
    # Init
    myDirs=dirsClass(start_dir,os.getcwd(),IncrLetter)
    mySummary=summaryLists()
    initVarVal={}

    # Now run the code
    varLoop(Vars,varOrder,0,initVarVal,myDirs,mySummary)
    mySummary.varsList=getVarList(Vars,varOrder)
    summarizeRuns(summary_file,mySummary)
