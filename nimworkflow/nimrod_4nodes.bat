#!/bin/bash -l
# Usage: sbatch cori.bat
# Run interactive: 
#     salloc --qos=interactive -Cknl,quad,cache -t30 -N8 -S4
#     .. then run the job script like: bash cori.bat

#SBATCH -N 4
#SBATCH -p regular
#SBATCH -C knl,quad,cache
#SBATCH -S 4      # use core specialization
#SBATCH --mail-user=xsli@lbl.gov
#SBATCH -t 01:30:00
#SBATCH --output=nimrod-4N.out #nlpkkt80-VecScatter+hugepages-8N.out

module load craype-hugepages2M
export NSUP=200

#OpenMP settings:
export OMP_PROC_BIND=spread
export OMP_PLACES=threads

# to prevent MKL from using 1 thread
export MKL_DYNAMIC=false     # Use threaded MKL
export OMP_DYNAMIC=false     

nprows=( 8 4 ) 
npcols=( 8 8 )  ## THREADS in  4 8

export MATDIR=/project/projectdirs/nimrod/jakeking/matrix
for MAT in nimrodMatrix-Te nimrodMatrix-Ti nimrodMatrix-V nimrodMatrix-B ; do
# for MAT in  nimrodMatrix-V nimrodMatrix-B ; do
  for ((i = 0; i < ${#npcols[@]}; i++)); do
    nprow=${nprows[i]}
    npcol=${npcols[i]}
    ntasks=$(($nprow*$npcol))
    nthreads=$((256/$ntasks))
    ncpus=$((${nthreads}*4))
    export OMP_NUM_THREADS=${nthreads}
    echo $ntasks " ntasks; " ${nthreads} " nthreads; " ${ncpus} " ncpus"
    srun -n ${ntasks} -c ${ncpus} --cpu_bind=cores ./pzdrive_mm -r ${nprow} -c ${npcol} ${MATDIR}/${MAT}
  done
done

# using -c = 8 is 4x slower
# Set -c value = "number of of logical cores (CPUs) per MPI task"
# srun -n 8 -c 32 --cpu_bind=cores ./pddrive_triple_v5 -r 2 -c 4 $MAT

#srun -n 8 -c 32 --cpu_bind=cores numactl -p 1 ./pddrive_rb_v5 -r 2 -c 4 $MAT
#srun -n 8 -c 32 --cpu_bind=cores ./pddrive_v4 -r 2 -c 4 g20.rua

# module load valgrind

# Add the following "sbcast" line here for jobs larger than 1500 MPI tasks:
# sbcast ./mycode.exe /tmp/mycode.exe

# Check Process and Thread Affinity
# export KMP_AFFINITY=verbose

# use mpirun if code is compiled using Intel MPI
# mpirun -np 8 ./pddrive_rb_v4 -r 2 -c 4 $MAT
