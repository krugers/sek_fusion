#!/bin/bash
#------------------------------------------------------------------
# Script: isetupenv.sh              Author: Scott Kruger
# Usage: isetupenv.sh  
# Description:  Change to a different version
#------------------------------------------------------------------
#     Check Usage
#------------------------------------------------------------------
usage_line="Usage: `basename $0` [--bash]"
if [ "$#" -gt 1 ]; then 
            echo $usage_line; exit;
elif [ "$#" -eq 1 ]; then 
      if [ "$1" == "--script" ]; then 
            SHELL="/bin/bash"
            SCRIPT_ARG="yes"
      fi
fi
source_file="sf"
#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
      source $topscriptdir/global_funcs.sh
else
      echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
host_info
#------------------------------------------------------------------
# Search for versions.  Requires NIMSOFT
#------------------------------------------------------------------
if test -z "$NIMSOFT"; then
   echo "You do not have NIMSOFT environment variable defined"
   echo "You need to either source the script created by the"
   echo "mknimall.sh script or use oldisetupenv.sh"
   exit
fi

vrs=`ls -d $NIMSOFT/nim* | grep -v sh`

#------------------------------------------------------------------
# Pick a version
#------------------------------------------------------------------
for version in $vrs
do
         new_version_name=`basename $version`
         versions="$versions $new_version_name"
done

###
##  For developers it's good to add the build dir to the list
#
if test -n "$NIMBLD"; then
	nimbld_root=`dirname $NIMBLD`
      #vrs=`ls -d $nimbld_root/* | grep -v sh`
      vrs=`ls -d $nimbld_root/../nim*/*/config.summary | sed 's/\/config.summary//g'`
	nimbld_root=`dirname $NIMBLD`
	for version in $vrs
	do
		   new_version_name=`basename $version`
               if test ${version:0:1} == "/"; then
                     new_version_name=`(cd $version; pwd)`
               fi
		   versions="$versions $new_version_name"
		   #versions="$versions $version"
	done
fi


###
##  Set up the menu
#
clear
echo ' '
echo ' '
echo ' '

while echo
do
  menu 'Choose new version ' $versions
  if test "${global_option:0:1}" == "/"; then
     new_nimvrs="$global_option"
     new_nimbld=$new_nimvrs
  else
     new_nimvrs="$NIMSOFT/$global_option"
     new_nimbld=$new_nimvrs
  fi
  if [ ! -x $new_nimbld/nimrod/nimrod-ser ] && [ ! -x $new_nimbld/bin/nimrod-ser ]; then
        echo "This build does not have valid nimrod executable."
        continue
  fi
  if test -e "$new_nimbld/share/config.summary"; then
        echo "Do you want to examine build parameters? (y/n)"
        read_yesno_response; build_yesno=$yn_response
        if test "$build_yesno" == "y"; then
          clear
          more $new_nimvrs/share/config.summary
          echo '===================================='
          echo; echo
          echo "Accept build version? (y/n)"
          read_yesno_response; accept=$yn_response
          if test "$accept" == "n"; then
                continue
          else
                break
          fi
        else
          break
    fi
  else
        break
  fi
done

#SEK: Should probably remove old path versions
if test -e $new_nimvrs/bin/nimrod; then
  nimrod=nimrod
elif test -e $new_nimvrs/bin/nimrod-ser; then
  nimrod=nimrod-ser
else
  echo "Cannot find nimrod executable"
  exit
fi

if test `basename $SHELL` == "tcsh" ||  test `basename $SHELL` == "csh" ; then
	cat > $source_file << EOSTCSH
setenv NIMVRS $new_nimvrs
setenv PATH $new_nimvrs/bin:${PATH}
rehash
rm sf
echo
echo "Now using nimrod at: "
echo \`which $nimrod\`
echo
EOSTCSH
elif test `basename $SHELL` == "bash"; then
	cat > $source_file << EOSBASH
NIMVRS=$new_nimvrs
PATH=$new_nimvrs/bin:$PATH
rm sf
echo
echo "Now using nimrod at: "
echo \`which $nimrod\`
echo
EOSBASH
else
	echo "$SHELL is not supported"
fi
if ! test "$SCRIPT_ARG" == "yes"; then
	echo "Type: source $source_file"
fi
