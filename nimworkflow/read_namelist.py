"""
 read_namelist.py: A module for reading and parsing namelists.

 A file containing 1 or more namelists is assumed to of the form:
 --------------------------- Begin file ----------------------------
 ! namelist header comments
 $namelist_name
    parameter = value   ! parameter comment
 $
 ; Another header comment
 &another_namelist_name
    another_parameter_value
 /
 Comments at the end of the file
 --------------------------- End file -------------------------------

NOTES ON REGEXP PARSING:
 As seen above have 3 types of characters we want to match:
    Beginning character: & or $
    Name characters: the word following the beginning char
    Ending pattern: Typically either $end or "/"
 E.g., &grid_input or $begin would have a beginning char of & or $ and
   we would want to match the grid_input or begin but ignore $end

 The patterns for matching each type of character described above are
 denoted at the top of this file for easy modification.
 
 To create the regular expressions for matching, use groups.
 Groups split up a regexp match and have the (...) notation.  
  (...)   implies match it and save it in a group
  (?:...) says match this group but don't capture it
  (?!...) is a "negative lookahead assertion" and says skip entirely

OUTPUT:
  Output from process_namelist is a python dictionary.
"""

import re, string, string_to_number, os, sys, glob

#---------------------------------------------------------------------
#  Customize patterns for detecting beginning and ending namelists and
#  for comments in the namelists
#---------------------------------------------------------------------
#Characters for beginning namelist
begin_pat=" [$&]"              # Put in brackets to protect

#Name pattern - I'm guessing what people use to denote namelists
name_pat="[a-zA-Z0-9_]+"            # + => 1 or more

#End pattern - end of a namelist sometimes denoted by either $end which
# causes problems because it will match so handle it here
# Also include other characters that mark the end of the namelist
end_pat="\$end|\&end"               # | => OR
end_pat=end_pat + "|[ \n][/]|\$[ \\n]"   # Need to include newline and space

# For comments in code
comment_pat="[!;]"

#---------------------------------------------------------------------
# Define patterns for slicing and dicing
#---------------------------------------------------------------------
# Pattern for finding namelist names
begin_group="(?:" + begin_pat + ")"       # Require, but don't include
name_group= "(" + name_pat + ")"    # Put pat into matching group
end_group="(?!" + end_pat +")"            # Ignore this ($end causes problems)
nl_name_pat=end_group + begin_group + name_group  # Negative assertion first

# Pattern for splitting up a file into parts
nl_split_pat="(" + begin_pat + name_pat + "|" + end_pat +")"

# Pattern for splitting up a line containing varval pair into parts
varval_split_pat="(?:=|"+comment_pat+")"
#---------------------------------------------------------------------

def process_namelist(filename):
      """
      process_namelist(namelist_name,text_block)
      Given a file or text string containing one or more namelists, split it
      up using the regexp module and then process each section.
      Returns a dictionary
      """
      # Check arguments
      namelist_dictionary = {}
      nl_string=check_for_namelist(filename)
      if nl_string == '': return namelist_dictionary

      #Important compiled regexp patterns
      name_parse=re.compile(nl_name_pat)   # for namelist names
      split_parse=re.compile(nl_split_pat) # to break up file into each part

      #This structure has everything we need nicely broken up
      nl_split=split_parse.split(nl_string)
      i=0
      while 1:
          #print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
          #print nl_split[i]
          #print "---------------------------------"
          header=clean_header_tailer(nl_split[i])
          i=i+1
          #print nl_split[i]
          #print "================================="
          nl_name=name_parse.search(nl_split[i]).group(1)
          i=i+1
          #print nl_split[i]
          #print "+++++++++++++++++++++++++++++++++"
          variable_dict,varinfo_dict = parse_vars(nl_split[i])
          i=i+1
          # Ignore the namelist termination
          i=i+1

          # Put into a dictionary structure.
          # Based on GA's read_namelist.py module
          namelist_dictionary[nl_name] = {}
          namelist_dictionary[nl_name]['header']   = header
          namelist_dictionary[nl_name]['variables']= variable_dict
          namelist_dictionary[nl_name]['varinfo']  = varinfo_dict
          
          # We might have a tailer
          if i == len(nl_split)-1:
              namelist_dictionary['tailer']=clean_header_tailer(nl_split[i])
              break
          
      return namelist_dictionary
      
def clean_header_tailer(ht_string):
      """clean_header_trailer(ht_string): given a typical header or
      trailer string which could have comment characters and extraneous
      white space, clean it up by removing them
      """
      # Remove comment characters that are at the beginning of the
      # line, but leave ones that exist elsewhere
      sub_pat='(?!\n)' + comment_pat
      ht_string=re.compile(sub_pat).sub("",ht_string)
      # Return string stripped of white space
      return ht_string.strip()

def parse_vars(namelist):
      """parse_vars(namelist): given a namelist as a string, separate
      into vars, values and comments where comments are denoted by 
      Returns two dictionaries: varval={var:val} and varinfo which
      contains comments, type, and converted value.
      """
      varval=re.compile(varval_split_pat)

      variables={}; varinfo={}
      # Loop over each line
      for line in re.compile("\n").split(namelist):
          # Ignore blank and commented lines
          if len(varval.split(line))>1 and varval.split(line)[0].strip() != '': 
              #Variable and value
              var,val=varval.split(line)[0].strip(),varval.split(line)[1].strip()

              # Converted value and type
              valtype = string_to_number.parse_number_string(val) #SEK: having problems with this
              # Variable comment
              if len(varval.split(line))>2:
                  com=varval.split(line)[2].strip()
              else:
                  com=''

              #Put into dictionary structures.
              variables[var]=val
              varinfo[var]=valtype
              varinfo[var]["comment"]=com
      return variables,  varinfo


def check_for_namelist(namelist_name):
      #SEK: This isn't working yet.
      """check_for_namelist(namelist_name,text_block):
      Check if either a namelist name or a text_block contains a
      namelist.  Return an empty string if it does not, return the
      string containing the namelists if it does."""
      #SEK: Note: should probably grep the strings to see if they
      # actually containing namelist names
      #SEK: Also need to generalize to two files? 
      source = ''; text_block=''
      if namelist_name != '' and text_block == '':
          source = 'file'
          try:
              namelist = open( namelist_name, 'r' )
              namelist_string=namelist.read()
          except IOError:
              print 'IOError: No such file or directory: ', namelist_name
              namelist_string=''
      elif text_block != '' and namelist_name == '':
          source = 'text'
          namelist_string=text_block
      elif text_block == '' and namelist_name == '':
          print 'Warning: No namelist source specified'
          namelist_string=''
      else:
          print 'Warning: Two namelist sources specified.'
          print 'Defaulting to file input.'
          print ''
          source = 'file'
          namelist_string=''
      
      #print 'namelist_name =', namelist_name
      #print 'source =', source
      return namelist_string

def process_files(filelist=None):
    """
    process_files: Do a namelist parsing on a list of files.  Returns a
    dictionary with the keys corresponding to the files.
    """
    success = 0; failures = []; retval = {}
    for file in filelist:
        if os.path.isfile(file):
          try:
              #print "Parsing file: ", file
              retval={}
              retval[file]=process_namelist(file)
              success += 1
          except:
              failures.append(file)
        else:
          print "File does not exist: ", file

   
    if failures:
        print '\nfailed while processing %s' % ', '.join(failures)
    #print '\nsucceeded on %d of %d files' %(success, len(filelist))

    return retval

if __name__ == "__main__":
      iarg=1
      if sys.argv[0].rfind("python")<0: iarg=0
      program_name=sys.argv[iarg]
      args=sys.argv[iarg+1:]
      print "program name ", program_name
      print args
      process_files(args)
