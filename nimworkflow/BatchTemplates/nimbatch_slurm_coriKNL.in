#!/bin/bash
#SBATCH -N @NNODE@
#SBATCH -C knl,quad,cache
#SBATCH -q @CLASS@
#SBATCH -S 4
#SBATCH -J @JOB_NAME@
#SBATCH -t 00:30:00

# Must be set
num_nodes=@NNODE@
run_type="procs"
export OMP_NUM_THREADS=4
exec=@NIMROD_EXE@

# Find the number of MPI procs
# There are 68 physical procs/nodes but we're skipping 4 with the -S 4
# option above
# Each processor is hyperthreaded for a total of 256 logical cores
mpi_procs=1
skip_procs=1
if test ${run_type} = "full"; then
  procs_per_node=256
elif test ${run_type} = "procs"; then
  procs_per_node=64
elif test ${run_type} = "half"; then
  procs_per_node=128
else
  echo "Unrecognized run_type"
  exit
fi
mpi_procs=`echo "${num_nodes} * ${procs_per_node} / ${OMP_NUM_THREADS}" | bc`
skip_procs=`echo "${num_nodes} * 256 / ${mpi_procs}" | bc`

# OpenMP settings:
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

# Additional settings:
export MKL_DYNAMIC=false # Use threaded MKL
export OMP_DYNAMIC=false
export NSUP=200 # For SuperLU_DIST

echo Beginning of Execution \`date\`

# run the application:
sargs="-n ${mpi_procs} -c ${skip_procs} --cpu_bind=cores"
echo "srun ${sargs}"
# Uncomment below to view the configuration
#srun ${sargs} check-hybrid.gnu.cori 
srun ${sargs} ${exec}

