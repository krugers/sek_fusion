#!/usr/bin/env python
import readBin
import os, optparse
import numpy
import math
from matplotlib import pylab

class growthRates:
    def __init__(self,data,nstep_use,fileType):
      """ Given the raw data from the other file, return:
        stepData[0:2, nstep_use] which has t,istep,dt
        modeData[0:1, nmodes] which has imode, keff
        depsData[nDepVars,nmodes, nsteps] which has the actual vars
      """
      self.fileType=fileType
      self.varList=readBin.getVarList(fileType)
      self.hasFrequencies=False
      self.hasGrowthRates=False
      nvar,nmodes,nsteps=data.shape
      if nstep_use>nsteps:
        print "Adjusting nsteps (requested ", nstep_use, \
              ") to file nsteps (", nsteps, ")"
        nstep_use=nsteps
      self.nvar=nvar; self.nmodes=nmodes; self.nsteps=nstep_use
      nIndVars=4
      nDepVars=nvar-nIndVars
      # Put time and istep in one array.  Add dt later
      self.stepData=numpy.zeros( (3,nstep_use), numpy.float)
      # Put imode and keff into another array
      self.modeData=numpy.zeros( (2,nmodes), numpy.float)
      self.depsData=numpy.zeros( (nDepVars,nmodes,nstep_use), numpy.float)

      # Copy over step #, time, imode, keff(imode)
      self.stepData[0,:]=data[0,0,nsteps-nstep_use:nsteps]
      self.stepData[0:2,:]=data[0:2,0,nsteps-nstep_use:nsteps]
      self.modeData[0:2,:]=data[2:4,:,0]
      # Reorder data for convenience
      for istep in range(nsteps-nstep_use,nsteps):
        i=istep-nsteps+nstep_use
        dt=data[1,0,istep]-data[1,0,istep-1]
        self.stepData[2,i]=dt
        self.depsData[:,:,i]=data[nIndVars:nvar+1,:,istep]


    def calcGrowthRates(self,taua,method="Dalton"):
      self.taua=taua
      nvar,nmodes,nsteps=self.depsData.shape
      self.hasFrequencies=False
      dt=self.stepData[2,:]
      nalloc=nsteps-1
      advar=0
      if self.fileType=="en":
        self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
        self.hasGrowthRates=True
        for i in range(1,self.nsteps):
          dlogf=numpy.log(self.depsData[:,:,i])-numpy.log(self.depsData[:,:,i-1])
          self.growthRates[:,:,i-1]=dlogf/(2.*dt[i])
      elif self.fileType=="nimhist":
        eval("self.calcGrowthRatesNimhist"+method+"()")
        # Reordered nimhist data so need to recalculate sized
        if self.hasGrowthRates:
          nvar,nmodes,nalloc=self.growthRates.shape
        else:
          nvar,nmodes,nalloc=self.frequencies.shape
        advar=3 # Add in places for B, V and total
      elif self.fileType=="logen":
        self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
        self.hasGrowthRates=True
        for i in range(1,self.nsteps):
          dlogf=(self.depsData[:,:,i]-self.depsData[:,:,i-1])/numpy.log10(numpy.e)
          self.growthRates[:,:,i-1]=dlogf/(2.*dt[i])
      elif self.fileType=="lnsen":
          nalloc=nsteps
          self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
          self.growthRates[:,:,:]=self.depsData[:,:,:]/2.
          self.hasGrowthRates=True
      if taua !=1.:
        growthRates=growthRates*taua
      # Do statistics on growth rates - initialize arrays
      # There are mean, standard deviation, # data points (Norm/N) 
      # and weight arrays.
      if self.hasGrowthRates:
        self.grMean       = numpy.zeros( (nvar+advar,nmodes), numpy.float)
        self.grMeanNorm   = numpy.zeros( (nvar+advar,nmodes), numpy.float)
        self.grStd        = numpy.zeros( (nvar+advar,nmodes), numpy.float)
        self.growthRatesW = numpy.zeros( (nvar,nmodes,nalloc), numpy.float)
      if self.hasFrequencies:
        self.freqMean     = numpy.zeros( (nvar+advar,nmodes), numpy.float)
        self.freqMeanN    = numpy.zeros( (nvar+advar,nmodes), numpy.float)
        self.freqStd      = numpy.zeros( (nvar+advar,nmodes), numpy.float)
        self.frequenciesW = numpy.zeros( (nvar,nmodes,nalloc), numpy.float)
      # Create weights array so average and Std ignore 0 elements.
      # This permits irregular arrays.
      if self.hasGrowthRates:
        for ivar in range(nvar):
          for imode in range(nmodes):
            for i in range(nalloc):
              if self.growthRates[ivar,imode,i] != 0:
                self.growthRatesW[ivar,imode,i]=1.0
            self.grMeanNorm[ivar,imode]=self.growthRatesW[ivar,imode,:].sum() 
      if self.hasFrequencies:
        for ivar in range(nvar):
          for imode in range(nmodes):
            for i in range(nalloc):
              if self.frequencies[ivar,imode,i] != 0:
                self.frequenciesW[ivar,imode,i]=1.0
            self.freqMeanN[ivar,imode]=self.frequenciesW[ivar,imode,:].sum()
      # Calculate statistical measures of growth rates
      for imode in range(nmodes):
        for ivar in range(nvar):
          if self.hasGrowthRates:
            if self.grMeanNorm[ivar,imode] != 0:
              self.grMean[ivar,imode]= \
                              numpy.average(self.growthRates[ivar,imode,:],
                                 0,weights=self.growthRatesW[ivar,imode,:])
              self.grStd[ivar,imode]=self.nonzeroStd( \
                self.growthRates[ivar,imode,:].flatten(), \
                self.grMean[ivar,imode],self.grMeanNorm[ivar,imode])
          if self.hasFrequencies:
            if self.freqMeanN[ivar,imode] != 0:
              self.freqMean[ivar,imode]= \
                                numpy.average(self.frequencies[ivar,imode,:],
                                   0,weights=self.frequenciesW[ivar,imode,:])
              self.freqStd[ivar,imode]=self.nonzeroStd( \
                self.frequencies[ivar,imode,:].flatten(), \
                self.freqMean[ivar,imode],self.freqMeanN[ivar,imode])
      # Now add in B, V, and total data.  Take advantage of the fact that
      # we can extend our previous averages and stdevs.
      if self.fileType=="nimhist":
        self.aggregateStatistics(nvar,0,3)
        self.aggregateStatistics(nvar+1,3,6)
        self.aggregateStatistics(nvar+2,0,nvar)
        nvar=nvar+3
        self.varList=self.varList+['B','V','Total']
      return 

    def calcGrowthRatesNimhistEric(self):
      """  Use Eric's method:  Find extreme and use those to find
           determine frequency and growth rates.
      """
      histData, modVarList=self.reorderNimhistData()
      self.varList=modVarList
      nvar,idum,nmodes,nsteps=histData.shape
      dt=self.stepData[2,:]
      next,extrema=self.findExtrema(histData)
      print "XXXXXXXX> ", next
      print "XXXXXXXX> ", extrema
      nalloc=next-2
      if next<=1:
        print "Could only find ",next," extrema.  "
        print "Cannot calculate growth rates or frequencies"
        self.hasGrowthRates=False
        return
      self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      self.frequencies=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      self.hasFrequencies=True
      for im in range(nmodes):
        for iv in range(nvar):
          for iext in range(2,next):
             t1=extrema[0,ivar,imode,iext]
             t0=extrema[0,ivar,imode,iext-2]
             f1=extrema[1,ivar,imode,iext]
             f0=extrema[1,ivar,imode,iext-2]
             self.growthRates[iv,im,iext-2]=numpy.log(f1/f0)/(t1-t0)
             self.frequencies[iv,im,i-1]=2.*numpy.pi/(t1-t0)
      self.hasGrowthRates=True
      return


    def calcGrowthRatesNimhistDalton(self):
      """  Dalton's formulas: 
      gamma = REAL( 2 * [ F(n+1) - F(n) ] / [ F(n+1) + F(n) ] /dt)
      omega = IMAG{ 2 * [ F(n+1) - F(n) ] / [ F(n+1) + F(n) ] /dt)
      F needs to be complex.
      """
      histData, modVarList=self.reorderNimhistData()
      self.varList=modVarList
      nvar,idum,nmodes,nsteps=histData.shape
      dt=self.stepData[2,:]
      t=self.stepData[1,:]
      nalloc=nsteps-1
      self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      self.frequencies=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      #DBG
      #for i in range(nsteps):
      #   histData[0,0,0,i]=10.**-2*numpy.real(numpy.exp(0.*t[i]+1j*30.*t[i]))
      #   histData[0,1,0,i]=10.**-2*numpy.imag(numpy.exp(0.*t[i]+1j*30.*t[i]))
      #pylab.plot(t,histData[0,0,0,:])
      #pylab.plot(t,histData[0,1,0,:])
      #pylab.show()
      #DBG
      for i in range(1,self.nsteps):
        for im in range(nmodes):
          for iv in range(nvar):
           df_m_r=histData[iv,0,im,i]-histData[iv,0,im,i-1]
           df_p_r=histData[iv,0,im,i]+histData[iv,0,im,i-1]
           df_m_i=histData[iv,1,im,i]-histData[iv,1,im,i-1]
           df_p_i=histData[iv,1,im,i]+histData[iv,1,im,i-1]
           # Get the complex numbers
           df_m=df_m_r+1j*df_m_i
           df_p=df_p_r+1j*df_p_i
           if df_p != 0:
             self.growthRates[iv,im,i-1]=2./dt[i]*numpy.real(df_m/df_p)
             self.frequencies[iv,im,i-1]=2./dt[i]*numpy.imag(df_m/df_p)
      self.hasGrowthRates=True
      self.hasFrequencies=True
      return

    def calcGrowthRatesNimhistCarl(self):
      """ Carl's formula:    
        gamma = (1/dt) Log[ Abs[f_(n+1)] / Abs[ f_n] ] 
        Abs[f_n] = Sqrt[ Re(f_n)^2 + Im(f_n)^2 ]
      f needs to be complex and only the real part of the growth
      rate is computed.
      """
      histData, modVarList=self.reorderNimhistData()
      self.varList=modVarList
      nvar,idum,nmodes,nsteps=histData.shape
      dt=self.stepData[2,1:nsteps]
      nalloc=nsteps-1
      absf=numpy.zeros((nvar,nmodes,nsteps), numpy.float)
      self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      absf=numpy.sqrt( numpy.square(histData[:,0,:,:])
                      +numpy.square(histData[:,1,:,:]))
      self.growthRates= \
        numpy.log(absf[:,:,1:nsteps]/absf[:,:,0:nsteps-1])/dt
      self.hasGrowthRates=True
      return

    def calcGrowthRatesNimhistBuneman(self):
      """  Use the Buneman method to find the growth and rotation 
      frequencies [JCP 29, 295 1978]. The formulas are
        F(n) = Re[f(n)]
        r^2 = [F(n+2)F(n) - F(n+1)^2]     / [F(n+1)F(n-1) - F(n)^2]
        2x  = [F(n+2)F(n-1) - F(n+1)F(n)] / [F(n+1)F(n-1) - F(n)^2]
        gamma = ln(r^2)/2dt
        omega = acos(2x^2/r^2-1)/2dt
      This algorithm appears to have problems with numerical precision. 
      This implementation looks a bit strange as it uses 
      f_n+x = f_n + df_n+x to eliminate the leading order f_n terms in 
      difference operations analytically.  
      """
      histData, modVarList=self.reorderNimhistData()
      self.varList=modVarList
      nvar,idum,nmodes,nsteps=histData.shape
      dt=self.stepData[2,1:nsteps-2]
      nalloc=nsteps-3
      self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      self.frequencies=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      # Use 128-bit floats
      fn    = numpy.zeros((nalloc), numpy.float128)
      dfnm1 = numpy.zeros((nalloc), numpy.float128)
      dfnp1 = numpy.zeros((nalloc), numpy.float128)
      dfnp2 = numpy.zeros((nalloc), numpy.float128)
      denom = numpy.zeros((nalloc), numpy.float128)
      rsq   = numpy.zeros((nalloc), numpy.float128)
      twox  = numpy.zeros((nalloc), numpy.float128)
      arg   = numpy.zeros((nalloc), numpy.float128)
      tiny = 1.e-25
      for iv in range(nvar):
        for im in range(nmodes):
          fn   = histData[iv,0,im,1:nsteps-2]
          rsq  = histData[iv,0,im,0:nsteps-3]
          dfnm1= rsq-fn
          rsq  = histData[iv,0,im,2:nsteps-1]
          dfnp1= rsq-fn
          rsq  = histData[iv,0,im,3:nsteps  ]
          dfnp2= rsq-fn
          denom= (dfnm1+dfnp1+dfnm1*dfnp1/fn)
          rsq  = (dfnp2-2.*dfnp1-dfnp1*dfnp1/fn)/denom
          twox = (dfnp2+dfnm1+dfnp2*dfnm1/fn-dfnp1)/denom
          arg  = \
            numpy.where(numpy.abs(rsq)>tiny,numpy.square(twox)/(2.*rsq)-1.,-2.)
          self.growthRates[iv,im,:]= \
            numpy.where(rsq>tiny,numpy.log(rsq)/(2.*dt),0)
          self.frequencies[iv,im,:]= \
            numpy.where(numpy.abs(arg)<=1,numpy.arccos(arg)/(2.*dt),0)
      self.hasGrowthRates=True
      self.hasFrequencies=True
      return
 
    def calcGrowthRatesNimhistZeros(self):
      """ Find rotation freq. from zero crossings for a function assumed 
      to vary as ~Sin[Im(gam)*t+phi]*Exp[Re(gam)*t] or ~Exp[gam*t] 
      where Im(gam) is the rotation freq. and Re(gam) is the growth rate.
      The advantage of this method is that it can find Im(gam) even for
      a case with keff=0. The disadvantage is that it requires many data 
      points. This could be called calcGrowthRatesNimhistCarl as Carl 
      introduced it to me (JRK).
      """
      histData, modVarList=self.reorderNimhistData()
      self.varList=modVarList
      nvar,idum,nmodes,nsteps=histData.shape
      nalloc=nsteps-1
      nzero=0
      time=self.stepData[1,:]
      self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      self.frequencies=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      for im in range(nmodes):
        for iv in range(nvar):
          nzero=0
          for i in range(1,self.nsteps):
            # Found a zero if true
            if (histData[iv,0,im,i]*histData[iv,0,im,i-1])<0:
              self.frequencies[iv,im,nzero]=time[i-1] \
                -histData[iv,0,im,i-1]*(time[i]-time[i-1]) \
                /(histData[iv,0,im,i]-histData[iv,0,im,i-1]) 
              nzero=nzero+1
          # Process zero points into frequencies
          if nzero >= 2:
            for i in range(1,nzero):
              self.frequencies[iv,im,i-1]=numpy.pi \
                /(self.frequencies[iv,im,i]-self.frequencies[iv,im,i-1])
            self.frequencies[iv,im,nzero-1]=0
          else:
            self.frequencies[iv,im,0]=0
      # We haven't found Re(gam)
      self.hasGrowthRates=False
      self.hasFrequencies=True        
      return


    def calcGrowthRatesNimhistCPhase(self):
      """ Find rotation freq. from the change in the complex phase of the 
      pertrubation. This only works for keff/=0 but it gives both sign and
      amplitude.  The formula is simple (with some logic to account for 
      the atan2 branch cut): Im(omega)=dphi/dt.
      """
      histData, modVarList=self.reorderNimhistData()
      self.varList=modVarList
      nvar,idum,nmodes,nsteps=histData.shape
      nalloc=nsteps-1
      nzero=0
      twopi=2*numpy.pi
      time=self.stepData[1,:]
      self.growthRates=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      self.frequencies=numpy.zeros((nvar,nmodes,nalloc), numpy.float)
      for im in range(nmodes):
        for iv in range(nvar):
          phi_last=math.atan2(histData[iv,1,im,0],histData[iv,0,im,0])
          for it in range(nalloc):
            # Compute (change in) complex phase
            phi=math.atan2(histData[iv,1,im,it+1],histData[iv,0,im,it+1])
            dphi=phi-phi_last
            phi_last=phi
            # Catch if we jumped the branch cut, range [-pi,pi]
            if math.fabs(dphi)>numpy.pi:
              if math.fabs(dphi+twopi) < math.fabs(dphi-twopi):
                dphi=dphi+twopi
              else:
                dphi=dphi-twopi
            # Now divide by dt to get Re(omega)
            self.frequencies[iv,im,it]=dphi/(time[it+1]-time[it])
      # We haven't found Re(gam)
      self.hasGrowthRates=False
      self.hasFrequencies=True
      return


    def findExtrema(self,histData):
      """  This finds the extrema of the variables using a polynomial
      fitting (similar to what Eric did in fortran).  Only operating on
      the real quantities
      """
      nvar,idum,nmodes,nsteps=histData.shape
      time=self.stepData[1,:]
      iext=0
      extArr=numpy.zeros((2,nvar,nmodes,nsteps),numpy.float)
      for ivar in range(nvar):
        for imode in range(nmodes):
          for istep in range(1,nsteps-1):
            imk=istep-1
            f0=histData[ivar,0,imode,istep-1]
            f1=histData[ivar,0,imode,istep]
            f2=histData[ivar,0,imode,istep+1]
            t1=time[istep]
            t2=time[istep+1]
            if (f1-f0)*(f2-f1)<0.:
              iext=iext+1
              extArr[0,ivar,imode,iext] = t1
              extArr[1,ivar,imode,iext] = f1
              print t1,f1
      return iext, extArr

    def reorderNimhistData(self):
      """  Because nimhist has real and complex data in a difficult to
           manipulate form, reorder it to make it easier to do
           calculations.  Get rid of currents and conc vars
      """
      nvars,nmodes,nsteps=self.depsData.shape
      # Note that we do not include the conc variable
      nvarsRed=(nvars-10)/2
      histData=numpy.zeros((nvarsRed,2,nmodes,nsteps), numpy.float)
      # First we have B
      modVarList=['Br','Bz','Bphi']
      histData[0:3,0,:,:]=self.depsData[0:3,:,:]
      histData[0:3,1,:,:]=self.depsData[3:6,:,:]
      #print  "HERE---------------------------------------"
      #print histData[0:3,0,:,:]
      #print  "HERE---------------------------------------"
      #print histData[0:3,1,:,:]
      #print  "HERE---------------------------------------"
      # Next 3 are real(J)
      #modVarList=modVarList+['Jr','Jz','Jphi']
      #histData[3:6,0,:,:]=self.depsData[6:9,:,:]
      #histData[3:6,1,:,:]=self.depsData[9:12,:,:]
      # Next 3 are real(V)
      modVarList=modVarList+['Vr','Vz','Vphi']
      histData[3:6,0,:,:]=self.depsData[12:15,:,:]
      histData[3:6,1,:,:]=self.depsData[15:18,:,:]
      # Next we have pressures 
      # Don't do density because that is frequently out-of-range
      modVarList=modVarList+['P','Pe']
      histData[6,0,:,:]=self.depsData[18,:,:]
      histData[6,1,:,:]=self.depsData[19,:,:]
      histData[7,0,:,:]=self.depsData[20,:,:]
      histData[7,1,:,:]=self.depsData[21,:,:]
      # Skip conc and then add the temperatures
      modVarList=modVarList+['Ti','Te']
      histData[8,0,:,:]=self.depsData[26,:,:]
      histData[8,1,:,:]=self.depsData[27,:,:]
      histData[9,0,:,:]=self.depsData[28,:,:]
      histData[9,1,:,:]=self.depsData[29,:,:]
      return histData, modVarList

    def aggregateStatistics(self,outvar,invar1,invar2):
      """ Compute aggregate statistics for additional quantities 
      such as total B, V and all data.  Assume arrays have been 
      correctly allocated such that out/in var locations exist.
      """
      if self.hasGrowthRates:
        nvar,nmodes=self.grMean.shape
        for imode in range(nmodes):
          self.grMeanNorm[outvar,imode]= \
            self.grMeanNorm[invar1:invar2,imode].sum(0)
          if self.grMeanNorm[outvar,imode] != 0:
            self.grMean[outvar,imode]= \
              (self.grMeanNorm[invar1:invar2,imode] \
              *self.grMean[invar1:invar2,imode]).sum(0) \
              /self.grMeanNorm[outvar,imode]
            self.grStd[outvar,imode]=self.nonzeroStd( \
              self.growthRates[invar1:invar2,imode,:].flatten(), \
              self.grMean[outvar,imode],self.grMeanNorm[outvar,imode])
      if self.hasFrequencies:
        nvar,nmodes=self.freqMean.shape
        for imode in range(nmodes):
          self.freqMeanN[outvar,imode]= \
            self.freqMeanN[invar1:invar2,imode].sum(0)
          if self.freqMeanN[outvar,imode] != 0:
            self.freqMean[outvar,imode]= \
              (self.freqMeanN[invar1:invar2,imode] \
              *self.freqMean[invar1:invar2,imode]).sum(0) \
              /self.freqMeanN[outvar,imode]
            self.freqStd[outvar,imode]=self.nonzeroStd( \
              self.frequencies[invar1:invar2,imode,:].flatten(), \
              self.freqMean[outvar,imode],self.freqMeanN[outvar,imode])
      return
    
    def nonzeroStd(self,inarr,mean,num):
      """ Compute the standard deviation but ignore zeros.
      Pass in the array, the pre-computed mean, and the size N (num).
      """
      meansq=numpy.square(mean)
      return numpy.sqrt(numpy.where(inarr!=0,numpy.square(inarr)-meansq,0.).sum()/num)

    def grDataPrint(self):
      nvar,nmodes,nsteps=self.growthRates.shape
      for imode in range(nmodes):
       print "keff(imode) = ", self.modeData[1,imode]
       for istep in range(nsteps):
         for i in range(nvar): print self.growthRates[i,imode,istep],
         print "\n",
      return

    def scriptPrint(self,nstep):
      nvar,nmodes,nstep_use=self.growthRates.shape
      if self.hasGrowthRates and self.hasFrequencies:
        for imode in range(nmodes):
          print self.modeData[1,imode] 
          print self.grMean[0,imode]
          print self.grStd[0,imode]
          print self.freqMean[0,imode]
          print self.freqStd[0,imode]
          print nstep
      elif self.hasGrowthRates:
        for imode in range(nmodes):
          print self.modeData[1,imode]," ",self.grMean[0,imode]," ", \
                self.grStd[0,imode]
      elif self.hasFrequencies:
        for imode in range(nmodes):
          print self.modeData[1,imode]," ",self.freqMean[0,imode]," ", \
                self.freqStd[0,imode]
      return

    def growthRatesPrint(self):
      if self.hasGrowthRates:
        nvar,nmodes=self.grMean.shape
      else:
        nvar,nmodes=self.freqMean.shape
      if self.taua ==1.:
        print "Growth rates given in units of s^-1"
      else:
        print "Growth rates given as Gamma Tau_A"
      if self.hasGrowthRates and self.hasFrequencies:
        print "Var".rjust(6),"   Growth (Re gamma)".ljust(29),
        print "Rotation (Im gamma)".ljust(24),"(Npts Re, Npts Im)"
      elif self.hasGrowthRates:
        print "Var".rjust(6),"   Growth (Re gamma)".ljust(30),"Npts"
      elif self.hasFrequencies:
        print "Var".rjust(6),"   Rotation (Im gamma)".ljust(30),"Npts"
      for imode in range(nmodes):
        print "keff =".rjust(8),self.modeData[1,imode]
        for ivar in range(nvar):
          if self.hasGrowthRates and self.hasFrequencies:
            print self.varList[ivar].rjust(6),":",
            print '{0:10.4e}'.format(self.grMean[ivar,imode]).rjust(11),"+/-",
            print '{0:10.4e}'.format(self.grStd[ivar,imode]).rjust(10),
            print '{0:10.4e}'.format(self.freqMean[ivar,imode]).rjust(11),"+/-",
            print '{0:10.4e}'.format(self.freqStd[ivar,imode]).rjust(10),
            print "(",'{0:4g}'.format(self.grMeanNorm[ivar,imode]),",",
            print '{0:4g}'.format(self.freqMeanN[ivar,imode]),")"
          elif self.hasGrowthRates:
            print self.varList[ivar].rjust(6),":",
            print '{0:10.4e}'.format(self.grMean[ivar,imode]).rjust(11),"+/-",
            print '{0:10.4e}'.format(self.grStd[ivar,imode]).rjust(10),
            print "(",'{0:4g}'.format(self.grMeanNorm[ivar,imode]),")"
          elif self.hasFrequencies:
            print self.varList[ivar].rjust(6),":",
            print '{0:10.4e}'.format(self.freqMean[ivar,imode]).rjust(11),"+/-",
            print '{0:10.4e}'.format(self.freqStd[ivar,imode]).rjust(10),
            print "(",'{0:4g}'.format(self.freqMeanN[ivar,imode]),")"
      return

    def plotGr(nstep_use,varList,grData,taua):
      nvar,nmodes,nsteps=grData.shape
      time=grData[1,0,:]
      for imode in range(nmodes):
        for ivar in range(4,nvar): 
          il=ivar-4
          plabel=varList[il]+" k="+str(grData[3,imode,0])
          pylab.plot(time,grData[ivar,imode,:],label=plabel)
      pylab.xlabel('time (s)')
      pylab.ylabel('growth rates (s^-1)')
      pylab.title("Growth rates versus time")
      pylab.legend(loc='upper left')
      pylab.show()
      return

def main():
    parser = optparse.OptionParser(usage="%prog [options] bin/txtfile")
    parser.add_option('-l', '--plot', dest='doPlot',
                      help='Make plot of the growth rate using nstep_use points', 
                      action='store_true')
    parser.add_option('-d', '--debug', dest='debug',
                      help='Print out debug information', 
                      action='store_true')
    parser.add_option('-n', '--nstep_use', dest='nstep_use', default='20',
                      help='Number of points used in averaging to calculate growth rate and plotting')
    parser.add_option('-t', '--taua', dest='taua', default='',
                      help='Normalize growth rates by Tau_A ')
    parser.add_option('-m', '--method', dest='method', default='Dalton',
                      help='Method of calculating the growth rate and frequencies of nimhist data. Options are Dalton, Eric, Zeros, Buneman, and CPhase (Zeros and CPhase give only frequencies).')
    parser.add_option('-s', '--scriptoutput', dest='script', 
                      help='Prints output for scripting',action='store_true')
    options, args = parser.parse_args()

    # Too many arguments
    if len(args) > 1 or len(args)==0:
      parser.print_usage()
      return
    else:
      dataFile=args[0]
    if options.taua != '':
      taua=numpy.float(options.taua)
    else:
      taua=numpy.float(1.)
    nstep_use=numpy.int(options.nstep_use)
    method=options.method

    # Get the data from the file
    fileType,data=readBin.read_nimout(dataFile)
    # For debugging
    #readBin.dataPrint(data)
    
    # Put into a common structure for all file types
    grdata=growthRates(data,nstep_use,fileType)

    grdata.calcGrowthRates(taua,method)

    #Debug
    #if not grdata.hasGrowthRates:
    #    return

    if options.debug:
        grdata.grDataPrint()

    if options.script:
      grdata.scriptPrint(data.shape[2])
    else:
      grdata.growthRatesPrint()

    if options.doPlot:
      grdata.plotGr()
      return

if __name__ == "__main__":
        main()


# This is the newton iteration for finding extrema based on a polynmial
# fitting
#SEK: Not sure why this doesn't work
#              print ">>>>>>>>> ", f0, f1, f2
#              pf=numpy.polyfit(time[imk:imk+3],histData[ivar,0,imode,imk:imk+3],2)
#              p=numpy.poly1d(pf); pd=numpy.polyder(p)
#              # Use a Newton iteration to find extrema
#              fold=f1; told=t1; tval=t1+(t2-t1)/2.; ilim=0
#              while 1:
#                 fval=numpy.polyval(p,tval)
#                 fder=numpy.polyval(pd,tval)
#                 print "XXX", ivar, imode, istep,"::: ",  tval, fder, fval, f1
#                 if abs(fder) < 1.e-12: break
#                 if ilim>10: break
#                 tval=told+(fval-fold)/fder
#                 ilim=ilim+1
#                 told=tval
#              extArr[0,ivar,imode,iext] = tval
#              extArr[1,ivar,imode,iext] = fval

