#!/bin/bash

#------------------------------------------------------------------
# Script: iplot.sh		Authors: Scott Kruger
# Usage: iplot.sh 
# Description:  
#	Interactive front end for batchplot.sh
#------------------------------------------------------------------
# Initialize variables
#------------------------------------------------------------------
nimplot_exe=$NIMSRC/nimrod/nimrod
manydump_exe=$NIMROOT/bin/manydump
tempfile_prefix="npin"
input_file="nimrod.in"
#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi

#------------------------------------------------------------------
# Menu for the interactive loop level
#------------------------------------------------------------------
function setup_plot_choices ()
{
	COLUMNS=1			# Only show 1 column for this menu
	choices=( )
	choices[1]='xy_slices of solution fields'
#	choices[2]='xt_slices of solution fields'
#	choices[3]='yt_slices of solution fields'
	choices[2]='contour plots of solution fields'
	choices[3]='vector plots of solution fields'
#	choices[6]='toroidal ave. safety factor and parallel current'
#	choices[7]='toroidal ave. poloidal flux contours'
	choices[4]='xy_slices vs. sqrt of poloidal flux'
#	choices[9]='energy spectra'
#	choices[10]='div(b) contours'
#	choices[11]='local mu0*J.B/B**2 contours'
#	choices[12]='<E>.<J> contours'
#	choices[13]='conductive heat flux vectors'
	choices[5]='exit'
}
#------------------------------------------------------------------
# Menu for the data representation loop level
#------------------------------------------------------------------
function setup_data_rep_choices ()
{
	choices=( )
	COLUMNS=1			# Only show 1 column for this menu
	choices[1]='plot one Fourier component'
	if [ $nonlinear = "t" ]; then
		choices[2]='plot point in configuration space'
		choices[3]='plot evenly spaced data in periodic coordinate.'
	fi
}
#------------------------------------------------------------------
# Menu for the data representation loop level
#------------------------------------------------------------------
function setup_mode_choices ()
{
	choices=( )
	COLUMNS=1			# Only show 1 column for this menu
	for ((ik=0; ik < $nmodes ; ik++))  # Double parentheses
	do
		choices[ik]="  keff($ik)=${keff[$ik]}"
	done
}
#------------------------------------------------------------------
# Handle the inquiry for the normal-tangential calculation
#------------------------------------------------------------------
function do_flux ()
{
	if [ $inquire_flux = "f" ]; then 
		flux_label="rz"
		return 
	fi

	echo "Convert poloidal field components to flux surface normal"
	echo " and tangential components? (y/n)"
	echo "This assumes that the grid is aligned with the flux"
	echo "surfaces, and the transformation is done in rblocks only!!!"
	while
	read response
	do
		case "$response" in
			"y")  flux_label="nt"
				let iline=iline+1; line[$iline]="y"
				break;;
			"n")  flux_label="rz"
				let iline=iline+1; line[$iline]="n"
				break ;;
			*)   continue;;
		esac
	done
}
#------------------------------------------------------------------
# Data representation level
#------------------------------------------------------------------
function data_rep()
{
	echo; echo
	echo "Data Options:"
	setup_data_rep_choices
	array_menu

	case $global_optnum in
	1)	# Mode plot
		let iline=iline+1; line[$iline]="o"
		setup_mode_choices
		if test ${#choices[@]} == 1; then
			global_optnum=1
		else
			echo; echo
			echo 'The following is a list of mode indices and wavenumbers'
			echo '(n for toroidal geometry or k=2*pi*n for linear geometry).'
			echo 'Select the index for the desired mode.'
			array_menu
		fi
		ik=`expr $global_optnum - 1`
		periodic_label=${keff[$ik]}
		periodic_flag="k"
		let iline=iline+1; line[$iline]="$ik"
		
		# If n=0, ask about the add_eq option
		if [ "$periodic_label" = "0" ]; then
           		echo 'Add the equilibrium fields to the perturbed fields'
           		echo 'and zero the equilibrium arrays for these plots? (y/n)'
			while
			read response
			do
				case "$response" in
					"y")  let iline=iline+1; line[$iline]="y"
						break;;
					"n")  let iline=iline+1; line[$iline]="n"
						break ;;
					*)   continue;;
				esac
			done
			echo
		fi

		# Ask about normal-tangential calculations
		do_flux
	  	;;
	2)	# Configuration plot
		let iline=iline+1; line[$iline]="c"
		echo; echo
		echo "Please enter the desired toroidal angle (in radians/2pi"
		echo " or z/per_length for linear geometry)."
		read response
		let iline=iline+1; line[$iline]="$response"
		periodic_label=`echo $response | sed 's/\.//'`
		periodic_flag="p"

		# If we are doing real space plots, I think add_eq
		# should always be y
		let iline=iline+1; line[$iline]="y"

		# Ask about normal-tangential calculations
		do_flux
	  	;;
	3)	# Periodically-spaced data plot
		let iline=iline+1; line[$iline]="p"
		echo; echo
		echo "Please enter the desired periodic coorindate increment"
		echo " (in radians/2pi or z/per_length for linear geometry)."
		read response
		let iline=iline+1; line[$iline]="$response"
		periodic_label=`echo $response | sed 's/\.//'`
		periodic_flag="p"

		# If we are doing real space plots, I think add_eq
		# should always be y
		let iline=iline+1; line[$iline]="y"

		# Ask about normal-tangential calculations
		do_flux
	  	;;
	esac
}
#------------------------------------------------------------------
# Write the input file that has been set up in the line array
#------------------------------------------------------------------
function write_input_file ()
{
	input_file_name=.$plot_label$flux_label$periodic_flag$periodic_label.npin
	if [ -e $input_file_name ]; then
		rm $input_file_name
	fi

	for ((il=1; il <= $iline ; il++))  # Double parentheses
	do
		echo ${line[il]} >> $input_file_name
	done

	#Exit nimplot cleanly: Return to plot level and type 0 to exit
	echo '' >> $input_file_name
	echo '' >> $input_file_name
	echo '' >> $input_file_name
	echo '' >> $input_file_name
	echo 0 >> $input_file_name

	#Null the line array
	iline=0; line=( )

}
#------------------------------------------------------------------
# Final messages and possibly other stuff
#------------------------------------------------------------------
function final_stuff
{	
	echo 
	echo 
	echo "Input file(s) of the form .*.npin have been created."
	echo "These files can be used by the batchplot script to"
	echo " automatically run nimplot."
	echo "Here are some examples of how to batchplot:"
	echo "            batchplot dump.01000 dump.01500"
	echo "            batchplot dump.*"
	echo 
	echo "To view the output of batchplot, use the pxd script"
	echo 
	echo 
}
#------------------------------------------------------------------
# START CODE EXECUTION HERE
#------------------------------------------------------------------
get_modes
clear
while echo
do
	echo
	echo "Please select plot option:"

	inquire_flux="t"
	setup_plot_choices
	array_menu
	plot_type=$global_optnum
	case "$plot_type" in
		# xy slices
		1)	line[1]=1
			line[2]="dump.xxxxx"; 	iline=2
			plot_label="xy"
			data_rep
			write_input_file
			;;
		# contour plots
		2)	line[1]=4
			line[2]="dump.xxxxx"
			line[3]="";			iline=3
			plot_label="con"
			data_rep
			write_input_file
			;;
		# vector plots
		3)	line[1]=5
			line[2]="dump.xxxxx"
			line[3]="";			iline=3
			plot_label="con"
			inquire_flux="f"
			data_rep
			write_input_file
			;;
		4)	line[1]=8
			line[2]="dump.xxxxx";	iline=2
			plot_label="xypf"
			data_rep
			write_input_file
			;;
		5)	final_stuff
			break;;
	esac
	clear
done
