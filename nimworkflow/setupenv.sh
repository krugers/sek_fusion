#!/bin/bash
#------------------------------------------------------------------
# Script: setupenv.sh  		Author: Scott Kruger
# Usage: setupenv.sh -nim $NIMROOT -vrs $NIMVRS -bld $NIMBLD 
# Description:  Set environment variables used by the other scripts.
# 	The environment variables are:
# 	   NIMROOT
# 	   NIMVRS
# 	   NIMSRC
# 	   NIMBLD
# Path is set up as well.  See notes below on how it is done.
#------------------------------------------------------------------
# Get options
while test -n "$1"; do
  case "$1" in
    -nim)
      set_nimroot=$2
      shift; shift
      ;;
    -vrs)
      set_nimvrs="$2"
      shift; shift
      ;;
    -bld)
      set_nimbld="$2"
      shift; shift
      ;;
    -shell)
      SHELL="$2"
      shift; shift
      ;;
    -script)
      SCRIPT_ARG="yes"
      shift; shift
      ;;
    -r)
      RESET=true
      shift
      ;;
    *)
      "Usage: $0 -vrs $NIMVRS -bld $NIMBLD [-nim $NIMROOT]"
      exit 1
      ;;
  esac
done

#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
#------------------------------------------------------------------
# global parameters for customization of script
#------------------------------------------------------------------
source_file="sf"
path_file="pathenv.sh"

#------------------------------------------------------------------
# START CODE EXECUTION HERE
#------------------------------------------------------------------
#------------------------------------------------------------------
# Error check.  Need to have both version and build set
#------------------------------------------------------------------
if test -z "$set_nimvrs"; then
	echo "Need to set version with -vrs"
	exit
fi
if test -z "$set_nimbld"; then
	echo "Need to set version with -bld"
	exit
fi
#------------------------------------------------------------------
# Determine source directory
#------------------------------------------------------------------
if test -d $set_nimvrs/Source; then
	set_nimsrc=$set_nimvrs/Source
else
	set_nimsrc=$set_nimvrs
fi

#------------------------------------------------------------------
# Determine drawdir.  Old way was to have it in source directory.
# Now it is in the version directory.
#------------------------------------------------------------------
if test -d $set_nimvrs/draw; then
	set_drawdir=$set_nimvrs/draw
elif test -d $set_nimsrc/draw; then
	set_drawdir=$set_nimsrc/draw
else
	echo "Cannot find draw directory in the following places:"
        echo $set_nimsrc
        echo $set_nimvrs
fi
#------------------------------------------------------------------
# Determine paths
# For historical/usage reasons, we want a hierarchy of preferences
#  for where to find nimrod.
#  Highest preference to low:
#    $NIMBLD/bin, $NIMVRS/bin, $NIMSRC/bin
#  Note that the latter is my old method.
#------------------------------------------------------------------

# By default, "make install" installs in NIMVRS.
# We could expect people to do an configure with a --prefix=$PWD
# but it is more convenient to assume they didn't
if test ! -e $set_nimbld/bin/nimrod; then
	make_version_bindir $set_nimbld
fi

# Do the version bin directory first if needed
if test "$set_nimvrs" != "$set_nimbld"; then
	if test -n $set_nimvrs; then
		current_nimvrs_bin=`echo $set_nimvrs/bin | tr / @`
		set_path=`echo $PATH | tr / @ | sed "s/$current_nimvrs_bin//g" | tr @ /`
		PATH="$set_nimvrs/bin:$set_path"
	fi
fi

# Test is to make sure we don't remove /bin.  That fucks up the path
if test -n $set_nimbld; then
	# Remove the old path. The tr is to get the seds to work nicely.
	current_nimbld_bin=`echo $set_nimbld/bin | tr / @`
	tmp_path=`echo $PATH | tr / @ | sed "s/$current_nimbld_bin//g" | tr @ /`
	set_path="$set_nimbld/bin:$tmp_path"
else 
	set_path=$PATH
fi

# Put variables into a file for when we are not in interactive use
cat > $topscriptdir/$path_file << EOS
NIMVRS=$set_nimbld
NIMSRC=$set_nimsrc
NIMBLD=$set_nimbld
DRAWDIR=$set_drawdir
EOS

# Setup for interactive use
if test `basename $SHELL` == "tcsh" ||  test `basename $SHELL` == "csh" ; then
	cat > $source_file << EOSTCSH
setenv NIMVRS $set_nimvrs
setenv NIMSRC $set_nimsrc
setenv NIMBLD $set_nimbld
setenv DRAWDIR $set_drawdir
setenv PATH $set_path
rehash
rm sf
echo
echo "Now using nimrod at: "
echo \`which nimrod\`
echo
EOSTCSH
elif test `basename $SHELL` == "bash"; then
	cat > $source_file << EOSBASH
NIMVRS=$set_nimvrs
NIMSRC=$set_nimsrc
NIMBLD=$set_nimbld
export NIMVRS NIMSRC NIMBLD
DRAWDIR=$set_drawdir
export DRAWDIR
PATH=$set_path
export PATH
rm sf
echo
echo "Now using nimrod at: "
echo \`which nimrod\`
echo
EOSBASH
else
	echo "$SHELL is not supported"
fi
if ! test "$SCRIPT_ARG" == "yes"; then
	echo "Type: source $source_file"
fi
exit
