#!/bin/bash

#------------------------------------------------------------------
# Script: ibatch.sh		Authors: Scott Kruger
# Usage: ibatch.sh
# Description:  Interactive front end for the batch.sh script
#------------------------------------------------------------------
# Variables for users to setup.
#------------------------------------------------------------------
batch_setup="batch.sh"			# Assumes it's in your path
sp_exe="iplot.sh"			# Assumes it's in your path
nimrun_options="no yes"
post_flag="no"
runnum=""
post=""
plot=""

#------------------------------------------------------------------
#	Check Usage
#------------------------------------------------------------------
usage_line="Usage: `basename $0` directories"
if [ "$#" -lt 1 ]; then echo $usage_line; exit; fi
#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
host_info "batch_set"


#------------------------------------------------------------------
# START CODE EXECUTION HERE
#------------------------------------------------------------------
clear; echo; echo

echo ' '
echo 'For the following menu choices, option 1 is your safest choice.'
echo ' '

menu 'Choose class of queue' $class_options
if test -n "$global_option"; then class="-queue $global_option"; fi

menu "Choose account to charge to $account_desc" $account_options
if test -n "$global_option"; then account="-repo $global_option"; fi

echo ' '
echo ' Input a label for your run'
echo ' '
read label
master_file=$label".in"
label="-name $label"

#------------------------------------------------------------------
#  Make the master namelist
#------------------------------------------------------------------
   echo " &master_input" > $master_file
   #------------------------------------------------------------------
   #  Go through and set each directory
   #------------------------------------------------------------------
   iscandir=0
   iproc=0
   for scandir in "$@"; do
   	let iscandir=$iscandir+1
   	echo 'Processing ' $scandir
   	cd $scandir
         echo "     subdirs("$iscandir")='"$scandir"'" >> ../$master_file
         echo "     inprocs("$iscandir")="$iproc       >> ../$master_file
   	# It's kind of weird, but it makes the comm split easy
         calc_nprocessors                              # Set NNODE, NPE, npe_charged
         if test -n "$SAVE_NPE"; then NPE=$SAVE_NPE; fi
   	let iproc=$iproc+$NPE
   	cd ..
   done
   # End
   echo " /" >> $master_file
   echo "!Total procs: "$iproc >> $master_file

   cp $master_file "master.in"

   npe_opt="-npe $iproc"
   if test -n "$NNODE"; then npe_opt="$npe_opt -nnode '$NNODE'"; fi
   if test -n "$taskline"; then npe_opt="$npe_opt -bc '$taskline'"; fi

#------------------------------------------------------------------
#  Now generate the batch file
#------------------------------------------------------------------
echo $batch_setup $class $account  $label $npe_opt
eval $batch_setup $class $account  $label $npe_opt
exit
