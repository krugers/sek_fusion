#!/bin/bash

#------------------------------------------------------------------
# Script: ibatch.sh		Authors: Scott Kruger
# Usage: ibatch.sh
# Description:  Interactive front end for the batch.sh script
#------------------------------------------------------------------
#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
host_info "batch_set"


#------------------------------------------------------------------
# START CODE EXECUTION HERE
#------------------------------------------------------------------
clear; echo; echo

echo ' '
echo ' Input a label for your run'
read label
scandir=$label

if [ -d $scandir ]; then
      	echo "Directory $new_dir exists.  Exiting"
		exit
fi
#------------------------------------------------------------------
#  Create the scan dir, copy over make_scandirs and label
#------------------------------------------------------------------
mkdir $scandir
cd $scandir
filescan=make_"$scandir"_dirs.py
cp $topscriptdir/template_makeScanDirs.py ./$filescan
cp $topscriptdir/many_nimset .
cp $topscriptdir/many_nimdraw .

cat << EOF

You are now ready to create your scandirs.  Your next steps:
cd $scandir
<edit $filescan>
$filescan -l $scandir <basedir>
imaster.sh *
make_nimset <dir_glob>
EOF


exit
