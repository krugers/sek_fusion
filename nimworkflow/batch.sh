#!/bin/bash
#------------------------------------------------------------------
# Script: batch.sh		Author: Scott Kruger
# Usage: batch.sh <submit>
# Description:  
#  Three purposes:
#    1. Take template file, and substitute in appropriate variables
#    2. Modify input file to work with queues
#  For 1, the variables are:
#       JOB_NAME - name of job.  I use directory name
#       ACCOUNT  - repository account
#       MT_HOURS - Length of time to run.  I use max for each queue
#       CLASS    - queue class (NERSC: regular, premium, ...)
#       NNODE    - # of nodes to have
#       NPE      - # of pes to have
#       TASKLINE - Extra batch commands to give
#       DIRECTORY- I always explicitly cd into $PWD
#       NIMROD_EXE - Line to run executable
#       PRE_NOTIFY  - Possibly email me about the run starting
#       POST_NOTIFY - Possibly email me about the run finishing
#       POST_PROCESS - Possibly post-process
#       POST_PLOTTING - Do any plotting needed
#       ENVSET - Any environment variables that need to be set
#  For 2, the namelist variables changed are:
#       cpu_tmax - Stop before end of queue (MT_HOURS - offset)
#  For 3, variables substituted are:
#       NUMBER_OF_RUNS
#       batch_file
#       batch_exe
#------------------------------------------------------------------
#
# USER'S SHOULD CHANGE THIS STUFF IF NEEDED:
#
#------------------------------------------------------------------
# I like full path
email=$MAIL_ADDRESS
# Subst'd variables.  These can be overwritten so act as defaults
DIRECTORY=$PWD
NUMBER_OF_RUNS="1"
JOB_NAME=`basename $PWD | tr '[a-z]' '[A-Z]'`
POST_PLOTTING='#nimdraw -xyk -conk dump*'
var=`which nimrod`
if test $var != ""; then
  NIMROD_EXE=$var
else
  NIMROD_EXE=$NIMVRS/bin/nimrod
fi
#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0`
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
nimrun_template=$topscriptdir/$nimrun.in
#------------------------------------------------------------------
# Dummy functions.  Source files will overwrite
#------------------------------------------------------------------
function calc_nprocessors { NNODE=1; NPE=1; }
function calc_times { max_time=0; MT_HOURS=0; }
#------------------------------------------------------------------
# Get options
# I need to know whether it is interactive or not in order to source 
# the right file, but the sourcing overwrites the variables.
# Use the SAVE_ notation to fix it.
#------------------------------------------------------------------
while test -n "$1"; do
  case "$1" in
    -i)
       interactive="yes";                          shift; ;;
    -l)
       launch="yes";                               shift; ;;
    -path)
       use_pathenv="yes";                          shift; ;;
    -repo)
      SAVE_ACCOUNT=$2;                                  shift; shift; ;;
    -queue)
      SAVE_CLASS="$2";                                  shift; shift; ;;
    -name)
      JOB_NAME="$2";                               shift; shift; ;;
    -time)
      SAVE_MT_HOURS="$2";                               shift; shift; ;;
    -npe)
      SAVE_NPE="$2";                                    shift; shift; ;;
    -nnode)
      SAVE_NNODE="$2";                                    shift; shift; ;;
    -bc)
      SAVE_TASKLINE="$2";                               shift; shift; ;;
    -exe)
      flagexe="y"; SAVE_NIMROD_EXE="$2";                shift; shift; ;;
    -post)
      post_flag="$2";                              shift; shift; ;;
    -notify)
      NOTIFY_FLAG="$2";                            shift; shift; ;;
    -plot)
      PLOT_FLAG="$2";                              shift; shift; ;;
    *)
      qflags="[-repo ACCOUNT] [-queue QUEUE] [-name JOB NAME] [ -time QUEUE TIME]"
      pflags="[-npe PROCESSOR ELEMENTS] [-bc BATCH COMMANDS]"
      xflags="[-exe EXECUTABLE LINE] [-post POST PROCESSING LINE]"
      ynflags="[-notify (y/n)] [-plot (y/n)]"
      "Usage: $0 $qflags $pflags $xflags $ynflags "
      exit 1
      ;;
  esac
done
echo '======================================================================================'
#------------------------------------------------------------------
# Machine specific parameters and functions
#------------------------------------------------------------------
if test "$interactive" == "yes"; then 
      batch_template="BatchTemplates/nimbatch_interactive.in" 
      source $topscriptdir/BatchConfig/batch_interactive.sh  
      if test "$launch" == "yes"; then 
        if test -z "$SAVE_NPE"; then calc_nprocessors; fi # Set NPE
        NIMROD_EXE="nohup mpirun -np $NPE $NIMROD_EXE 1> \$jobfilename 2\>\&1 \&" 
      else 
        NIMROD_EXE="nohup $NIMROD_EXE 1> \$jobfilename 2\>\&1 \&" 
      fi 
else
      host_info "batch_set"
fi
#------------------------------------------------------------------
# Allow for grabbing pathenv.sh
#------------------------------------------------------------------
if test "$use_pathenv" == "yes"; then
	if test -e $NIMROOT/pathenv.sh; then
		source $NIMROOT/pathenv.sh
		if  test -z $flagexe; then
                  # Reset with new path
                  NIMROD_EXE=$NIMVRS/bin/nimrod
            fi
	else
		echo "You requested to use pathenv.sh but I can't find it."
		echo "Exitting."
		exit
	fi
fi

#------------------------------------------------------------------
#  Go back and restore commandline options
#------------------------------------------------------------------
if test -n "$SAVE_ACCOUNT"; then ACCOUNT=$SAVE_ACCOUNT; fi
if test -n "$SAVE_CLASS"; then CLASS=$SAVE_CLASS; fi
if test -n "$SAVE_TASKLINE"; then TASKLINE=$SAVE_TASKLINE; fi
if test -n "$SAVE_NIMROD_EXE"; then NIMROD_EXE=$SAVE_NIMROD_EXE; fi
#------------------------------------------------------------------
# Try to determine build of nimrod by copying over build directory
#------------------------------------------------------------------
if test -e $NIMVRS/bin/$config_summary; then
	cp $NIMVRS/bin/$config_summary .
elif test -e $NIMVRS/$config_summary; then
	cp $NIMVRS/$config_summary .
elif test -e $NIMVRS/share/$config_summary; then
	cp $NIMVRS/share/$config_summary .
fi
#------------------------------------------------------------------
# Create batch file based on values and templates
# Some variables are critical, so test for them.
#------------------------------------------------------------------
function create_batchfile
{	
	if test -z "$NNODE" && test -n "$interactive" ; then
		echo "NNODE not properly defined.  Exiting"; exit
	fi

	sed "
	:t
	s,@JOB_NAME@,$JOB_NAME,;t t
	s,@ACCOUNT@,$ACCOUNT,;t t
	s,@MT_HOURS@,$MT_HOURS,;t t
	s,@CLASS@,$CLASS,;t t
	s,@TASKLINE@,$taskline,;t t
	s,@DIRECTORY@,$DIRECTORY,;t t
	s,@PRE_NOTIFY@,$PRE_NOTIFY,;t t
	s,@POST_NOTIFY@,$POST_NOTIFY,;t t
	s,@POST_PROCESS@,$POST_PROCESS,;t t
	s,@POST_PLOTTING@,$POST_PLOTTING,;t t
	s,@NIMRUN_LINE@,$NIMRUN_LINE,;t t
	s,@NNODE@,$NNODE,;t t
	s,@NPE@,$NPE,;t t
	s,@NIMROD_EXE@,$NIMROD_EXE,;t t
	s,@ENVSET@,$envset,;t t
	" $topscriptdir/$batch_template > $batch_file

	chmod u+x $batch_file
}
#------------------------------------------------------------------
# Provenance: create a summary file to keep track of what's done
#------------------------------------------------------------------
function print_batchsummary
{
	# Use grep to make sure that we're really doing what we want
	get_namelist_val cpu_tmax;  nimrod_mt=$var_value
	get_namelist_val tmax;      nimrod_tmax=$var_value
	get_namelist_val nstep;     nimrod_nstep=$var_value

      echo                                                   | tee $run_summary_file
	echo "      RUN SUBMITTAL SUMMARY "                    | tee -a $run_summary_file
	echo "          Date:" `date`                          | tee -a $run_summary_file
	echo " Number of Processors:        \
			${bluef}${yellowb}$NPE${reset}"    		 | tee -a $run_summary_file
	if test -n "$npe_charged"; then
	echo " Number of Processors Charged:\
			${bluef}${yellowb}$npe_charged${reset}"    | tee -a $run_summary_file
	fi
	if test -n "$NNODE"; then
	echo " Number of Nodes:             \
			${bluef}${yellowb}$NNODE${reset}"       	| tee -a $run_summary_file
	fi
	echo " Max. run time in queue (s):\
			${bluef}${yellowb}$max_time${reset}"    | tee -a $run_summary_file
	echo " Max. run time in queue (hrs):\
			${bluef}${yellowb}$MT_HOURS${reset}"    | tee -a $run_summary_file
	echo " Time limit in nimrod.in (s):\
			${bluef}${yellowb}$nimrod_mt${reset}"   | tee -a $run_summary_file
	echo " Max. # of steps in nimrod.in:\
			${bluef}${yellowb}$nimrod_nstep${reset}" | tee -a $run_summary_file
	echo " Max. time in nimrod.in (s):\
			${bluef}${yellowb}$nimrod_tmax${reset}"  | tee -a $run_summary_file
	echo  | tee -a $run_summary_file
	echo " nimrod executable used:"   			     | tee -a $run_summary_file
	echo "	${redf}$NIMROD_EXE"${reset}              | tee -a $run_summary_file
	echo  | tee -a $run_summary_file
	echo "When you are ready to submit your job, type ${redf}$NIMRUN_LINE${reset}." | tee -a $run_summary_file
	echo | tee -a $run_summary_file
	echo "This information can be found in the file: ${greenf}$run_summary_file${reset}"
	echo
}
	
#------------------------------------------------------------------
# Provenance: create a summary file to keep track of what's done
#------------------------------------------------------------------
function print_interactivesummary
{
	# Use grep to make sure that we're really doing what we want
	get_namelist_val cpu_tmax;  nimrod_mt=$var_value
	get_namelist_val tmax;      nimrod_tmax=$var_value
	get_namelist_val nstep;     nimrod_nstep=$var_value

      echo                                                   | tee $run_summary_file
	echo "      RUN SUBMITTAL SUMMARY "                    | tee -a $run_summary_file
	echo "          Date:" `date`                          | tee -a $run_summary_file
	echo " Time limit in nimrod.in (s):\
			${bluef}${yellowb}$nimrod_mt${reset}"    | tee -a $run_summary_file
	echo " Max. # of steps in nimrod.in:\
			${bluef}${yellowb}$nimrod_nstep${reset}" | tee -a $run_summary_file
	echo " Max. time in nimrod.in (s):\
			${bluef}${yellowb}$nimrod_tmax${reset}"  | tee -a $run_summary_file
	echo
	echo " nimrod executable used:"   			     | tee -a $run_summary_file
	echo "	${redf}$NIMROD_EXE"${reset}              | tee -a $run_summary_file
	echo  | tee -a $run_summary_file
	echo "When you are ready to submit your job, type ${redf}${NIMRUN_LINE}${reset}." | tee -a $run_summary_file
	echo | tee -a $run_summary_file
	echo "This information can be found in the file: ${greenf}$run_summary_file${reset}"
	echo
}
	
#------------------------------------------------------------------
#  START EXECUTION HERE
#------------------------------------------------------------------
if test -z "$SAVE_NPE"; then calc_nprocessors; fi # Set NNODE, NPE, npe_charged
calc_times                                        # Set MT_HOURS, max_time
if test -n "$SAVE_MT_HOURS"; then MT_HOURS=$SAVE_MT_HOURS; fi
if test -n "$SAVE_NPE"; then NPE=$SAVE_NPE; fi
if test -n "$SAVE_NNODE"; then NNODE=$SAVE_NNODE; fi
NIMRUN_LINE="$batch_exe $batch_file"
create_batchfile
nimrod_mt=`expr $max_time - $max_time_offset` #  Offset defined in batch_*.sh
# Put interactive jobs in background
if test -n "$interactive"; then batch_file="\"$batch_file \&\""; fi

echo "-------------------------------------------------------"
initializeANSI
if test -n "$interactive"; then
      print_interactivesummary
else
	modify_namelist "cpu_tmax" $nimrod_mt
      print_batchsummary
fi
echo "-------------------------------------------------------"
