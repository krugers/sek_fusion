#!/bin/bash
#------------------------------------------------------------------
# Script: mknewrun.sh        		Author: Scott Kruger
# Usage: mknewrun.sh <old_dir> <new_dir>
# Description:  This script is for automatically creating a new
#		  run based on an old run
#------------------------------------------------------------------
#------------------------------------------------------------------
#  Idiot checks for arguments and existence of files and directory
#------------------------------------------------------------------
usage_line="Usage: `basename $0` [-noedit] [-nointeract] <old_dir> <new_dir>"
nocheck="no"
if test ${#@} -eq 0; then
      echo $usage_line; exit
else
	while test -n "$1"; do
	  case "$1" in
	    -noedit)
		noedit="yes";                                shift; ;;
	    -nointeract)
		nointeract="yes"; noedit="yes";              shift; ;;
	    -nocheck)
		nocheck="yes";                               shift; ;;
	    -first)
	     default_dump="first";                         shift; ;;
	    -last)
	     default_dump="last";                          shift; ;;
	    -none)
	     default_dump="none";                          shift; ;;
	    -help)
		echo $usage_line;                            exit ;;
	    *)
		old_dir=`dirname $1`/`basename $1`  # Remove any slashes from tab completion
		new_dir=$2
		break ;;
	  esac
	done
fi


if [ ! -d $old_dir ]; then
	echo $old_dir "does not exist.  Exiting"
	exit 1
fi
last_file=`ls $old_dir/*nimrod.in | tail -1`
if  test ! -e $last_file && test -z "$nointeract"; then
	echo "$old_dir/nimrod.in does not exist.  Exiting"
	exit
fi

#------------------------------------------------------------------
# Work from old_dir for ease of testing.  
# Use absolute paths
#------------------------------------------------------------------
current_dir=$PWD
if [ -d $new_dir ]; then
  if test "$nocheck" == "no"; then
	if [ -n "$nointeract" ]; then
      	echo "Directory $new_dir exists.  Exiting"
		exit
	else
		echo "Directory $new_dir exists.  Proceed?"
		read_yesno_response; proceed_yesno=$yn_response
		if test "$build_yesno" == "n"; then exit;  fi
	fi
   fi
else
	mkdir $new_dir
fi
# Get full path
cd $new_dir
new_dir=`pwd`
cd $current_dir

#------------------------------------------------------------------
# Global nimrod functions
#------------------------------------------------------------------
topscriptdir=`dirname $0` 
if test -e $topscriptdir/global_funcs.sh; then
	source $topscriptdir/global_funcs.sh
else
	echo "Cannot find global_funcs.sh.  Exiting.";  exit
fi
#------------------------------------------------------------------
# Define files to copy over
#------------------------------------------------------------------
# Go to the directory to figure out which directories to copy over
cd $old_dir
shellscripts=`ls *.sh 2> /dev/null | grep -v ^run `
cpfiles=`ls *nim*.in`
cpfiles="$cpfiles fluxgrid.in mode.list cel.in $shellscripts"
runname=`basename $PWD`
diff_file=$runname.diff
cpfiles="$cpfiles $diff_file"

# Copy first dump file by default
if ls dump* > /dev/null 2>&1; then
	first_dump=`ls dump.* | head -1`
	last_dump=`ls dump.* | tail -1`

      # Make first dump the default unless told to do no dump
	global_option="$first_dump"

	if test -n "$default_dump"; then
		if [ "$default_dump" = "first" ]; then 
			global_option="$first_dump"
		elif [ "$default_dump" = "last" ]; then 
			global_option="$last_dump"
            else
			global_option="none"
		fi
	else
		# Prompt for menu if we haven't figured it out
		if test -z "$nointeract" && test "$first_dump" != "$last_dump"; then
			menu "Choose first or last dump file" $first_dump $last_dump none
		fi
	fi

	if test "$global_option" != "none"; then cpfiles="$cpfiles $global_option"; fi
fi

# Find GS file if appropriate
if [ -e "fluxgrid.in" ]; then
      get_namelist_val "filename" "fluxgrid.in"
      eqfile=$var_value
      baseeqfile=`basename $eqfile`
      if test "$eqfile" == "$baseeqfile"; then
	  cpfiles="$cpfiles $eqfile"
      fi
fi

#------------------------------------------------------------------
#  Make new directory and copy relevent files from old directory
#------------------------------------------------------------------
for file in $cpfiles; do
	copy_file $file $new_dir
done

#------------------------------------------------------------------
# Do stuff in new_dir if needed
#------------------------------------------------------------------
initializeANSI
cd $new_dir

fix_comment_line `basename $PWD` `basename $old_dir`
if test -z "$noedit"; then nimdex -noeditnotes; fi

if [ -z $nointeract ]; then
  echo 
  if which nimrod  > /dev/null 2>&1 ; then
	echo "You are currently using this version of nimrod:"
	echo "	${redf}`which nimrod`"${reset}
	echo 
	echo "If this is incorrect, type 'isetupenv.sh'"
  else
	echo "Before setting up run, you need to set the version of"
	echo "nimrod to use.  Type 'isetupenv.sh'"
  fi
  echo 
  echo "When ready,  you need to"
  echo "   ${redf} cd `basename $new_dir`"
  echo "   ibatch.sh"${reset}
fi

