batch_exe=qsub
batch_file="run_batch.pbs"

# Nimrod's time limit=queue_limit - max_time_offset (seconds)
max_time_offset=2500	

# I'm assuming that the generic has only one queue and no accounts
class_options="batch debug low"
account_options="mp21 mp290 mp200 mp94 m275"
account_desc="mp21=NIMROD mp290=SAIC mp200=UW-M mp94=GA"
#------------------------------------------------------------------
# Set NNODE, NPE, npe_charged
# Need better heuristics based on performance.  See Carls
#------------------------------------------------------------------
function calc_nprocessors 
{
	get_namelist_val nxbl
	nxbl=$var_value
	get_namelist_val nybl
	nybl=$var_value
	get_namelist_val nlayers
	nlayers=$var_value

	NPE=`expr $nxbl \* $nybl \* $nlayers`
	if [ "$NPE" = "" ];  then
	   echo "Problem with one of the following parameters.  See nimrod.in"
	   echo "nxbl: " $nxbl
	   echo "nybl: " $nybl
	   echo "nlayers: " $nlayers
	   exit
	fi

	remainder=`expr $NPE % 2`
	if [ "$remainder" = "0" ]; then
		  NNODE=`expr $NPE \/ 2`
	else
		  NNODE=`expr $NPE \/ 2 + 1`
	fi
}

#------------------------------------------------------------------
# Given queue and NPE, calculate the maximum length of time we have for
# the queue, and then set NIMROD's run time
#------------------------------------------------------------------
function calc_times
{
	if [ "$CLASS" = "batch" ];  then
		if [ "$NNODE" -le 16 ]; then
			MT_HOURS=48:00:00
			max_time=172800
		elif [ "$NNODE" -le 32 ]; then
			MT_HOURS=24:00:00
			max_time=86400
		elif [ "$NNODE" -le 64 ]; then
			MT_HOURS=12:00:00
			max_time=43200
		else
			MT_HOURS=06:00:00
			max_time=21600
		fi
	elif [ "$CLASS" = "low" ];  then
		MT_HOURS=12:00:00
		max_time=43200
	elif [ "$CLASS" = "debug" ];  then
		MT_HOURS=00:30:00
		max_time=4300
	else
		echo "Specify max. residency time manually"
		MT_HOURS=CHANGE_THIS
	fi
}
#-------------------------------------------------------------------
#  Useful info from NERSC
#-------------------------------------------------------------------
# Batch queues
# 
# There are four submit queues on Jacquard. The submit queues will route your
# job to the correct execution queue based on its requirements. The lower the
# relative priority of the queue the higher the actual priority of the jobs in
# the queue as far as the scheduler is concerned. Other things being equal a job
# in a queue with a relative priority of n will be scheduled ahead of one in a
# queue with a relative priority of n+1.
#
# Submit 	Exec 					Relative
# Queue 	 Queue 		Nodes 	Max Wallclock 	Priority
# ==========	===========	=====	=============	========
#
# interactive 	interactive 	1-16 	30 mins 	1
#
# debug 	debug 		1-32 	30 mins 	2
#
# batch 	batch16 	1-16 	48 hours 	7
# 		batch32 	17-32 	24 hours 	5
# 		batch64 	33-64 	12 hours 	4
# 		batch128 [2] 	65-128 	6 hours 	6
# 		batch256 [3] 	129-256 6 hours 	3
#
# low 		low 		1-64 	12 hours 	8
#
# Notes:
# 
# 1 There is a maximum of 4 running jobs per user over the whole system.
#   Interactive and debug queues only allow one job per user
# 2 Only one batch128 job is allowed to run at a time.
# 3 The batch256 queue will usually have a run limit of zero. 
#   NERSC staff will monitor this queue and make special arrangements to 
#   run jobs of this size. 
