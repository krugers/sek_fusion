batch_exe=qsub
batch_file="run_batch.pbs"
# Nimrod's time limit=queue_limit - max_time_offset (seconds)
max_time_offset=2500	
# I'm assuming that the generic has only one queue and no accounts
#class=regular
#class_options="regular premium low debug"
#account=mp21
#account_options="mp21 mp290 mp200 mp94 m275"
#account_desc="mp21=NIMROD mp290=SAIC mp200=UW-M mp94=GA"
#------------------------------------------------------------------
#function set_long_or_short
#{	
#		echo "Normally we set the regular queue for 12 hours, "
#		echo "but there is a long queue if for 24 hours."
#	 	echo "Do you wish this to be in the long queue? (y/n)"
#		menu "no yes"
#		response=$global_option
#		case "$response" in
#			"yes")	MT_HOURS=24:00:00
#					max_time=86400
#					;;
#			"no")		MT_HOURS=12:00:00
#					max_time=43200
#					;;
#		esac
#}
#------------------------------------------------------------------
# Set NNODE, NPE, npe_charged
# Need better heuristics based on performance.  See Carls
#------------------------------------------------------------------
function calc_nprocessors 
{
	get_namelist_val nxbl
	nxbl=$var_value
	get_namelist_val nybl
	nybl=$var_value
	get_namelist_val nlayers
	nlayers=$var_value

	NPE=`expr $nxbl \* $nybl \* $nlayers`
	if [ "$NPE" = "" ];  then
	   echo "Problem with one of the following parameters.  See nimrod.in"
	   echo "nxbl: " $nxbl
	   echo "nybl: " $nybl
	   echo "nlayers: " $nlayers
	   exit
	fi
}
#------------------------------------------------------------------
# Given queue and NPE, calculate the maximum length of time we have for
# the queue, and then set NIMROD's run time
#------------------------------------------------------------------
function calc_times
{
	MT_HOURS=48:00:00
	max_time=172800
}
