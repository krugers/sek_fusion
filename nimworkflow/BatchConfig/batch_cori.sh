batch_exe=qsub
batch_file="run_batch.slurm"

# http://www.nersc.gov/users/computational-systems/cori/running-jobs/queues-and-policies/

# Nimrod's time limit=queue_limit - max_time_offset (seconds)
max_time_offset=2500	
CLASS=regular
			#CLASS=premium
			#CLASS=debug
class_options="regular premium debug"
#class_options="regular premium debug shared"


account=mp21
			#account=mp21
			#account=mp200
account_options="mp21 mp200 mp94 m275 m876"
account_desc="mp21=NIMROD mp200=UW-M mp94=GA"

#PRE_NOTIFY="mailx -s "Starting \$job_name on \$HOST" $MAIL_ADDRESS < $input_file"
#POST_NOTIFY="mailx -s "Starting \$job_name on \$HOST" $MAIL_ADDRESS < $input_file"


#------------------------------------------------------------------
# Set NNODE, NPE, npe_charged
# Need better heuristics based on performance.  
#------------------------------------------------------------------
function calc_nprocessors 
{
	get_namelist_val nxbl
	nxbl=$var_value
	get_namelist_val nybl
	nybl=$var_value
	get_namelist_val nlayers
	nlayers=$var_value

	NPE=`expr $nxbl \* $nybl \* $nlayers`
	if [ "$NPE" = "" ];  then
	   echo "Problem with one of the following parameters.  See nimrod.in"
	   echo "nxbl: " $nxbl
	   echo "nybl: " $nybl
	   echo "nlayers: " $nlayers
	   exit
	fi

	remainder=`expr $NPE % 2`
	if [ "$remainder" = "0" ]; then
		  NNODE=`expr $NPE \/ 2`
	else
		  NNODE=`expr $NPE \/ 2 + 1`
	fi
}

#------------------------------------------------------------------
# Given queue and NPE, calculate the maximum length of time we have for
# the queue, and then set NIMROD's run time
#------------------------------------------------------------------
function calc_times
{
	if [ "$CLASS" = "regular" ];  then
		MT_HOURS=48:00:00
		max_time=172800
	elif [ "$CLASS" = "premium" ];  then
		MT_HOURS=48:00:00
		max_time=172800
	elif [ "$CLASS" = "debug" ];  then
		MT_HOURS=00:30:00
		max_time=4300
		#Account for offset max_time=1800
	else
		echo "Specify max. residency time manually"
		MT_HOURS=CHANGE_THIS
	fi
}
#-------------------------------------------------------------------
#  Useful info from NERSC
#-------------------------------------------------------------------
#SEK: Is this right?
# Submit Class	Execution Class	 Available PEs	 Max Wallclock	Relative Priority
# interactive	 interactive	1-256		30 mins		1
# debug 	 debug	1-256	30 mins		2		1
# premium 	 premium	1-8192		12 hrs		3
# regular	 reg_small	1-4094		12 hrs		5
# regular   	 reg_big	2048-6143	12 hrs		4

