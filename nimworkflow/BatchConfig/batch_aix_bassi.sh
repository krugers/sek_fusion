#Set defaults
batch_exe=llsubmit
batch_file="run_batch.ll"
# Nimrod's time limit=queue_limit - max_time_offset (seconds)
max_time_offset=2500	
CLASS=regular
			#CLASS=low
			#CLASS=premium
			#CLASS=regular_long
			#CLASS=debug
class_options="regular premium low debug"

account=mp21
			#account=mp21
			#account=mp200
account_options="mp21 mp200 mp94 m275"
account_desc="mp21=NIMROD mp200=UW-M mp94=GA"

#PRE_NOTIFY="mailx -s "Starting \$job_name on \$HOST" $MAIL_ADDRESS < $input_file"
#POST_NOTIFY="mailx -s "Starting \$job_name on \$HOST" $MAIL_ADDRESS < $input_file"

#------------------------------------------------------------------
# Idiot check - don't run if nimrod.in isn't here
#------------------------------------------------------------------
if test ! -e "nimrod.in"; then
  echo "nimrod.in doesn't exist.  I think you are in the wrong directory"
fi

#------------------------------------------------------------------
#function set_long_or_short
#{	
#		echo "Normally we set the regular queue for 12 hours, "
#		echo "but there is a long queue if for 24 hours."
#	 	echo "Do you wish this to be in the long queue? (y/n)"
#		menu "no yes"
#		response=$global_option
#		case "$response" in
#			"yes")	MT_HOURS=24:00:00
#					max_time=86400
#					;;
#			"no")		MT_HOURS=12:00:00
#					max_time=43200
#					;;
#		esac
#}
#------------------------------------------------------------------
# Set NNODE, NPE, npe_charged
# Need better heuristics based on performance.  See Carls
#------------------------------------------------------------------
function calc_nprocessors 
{
	get_namelist_val nxbl
	nxbl=$var_value
	get_namelist_val nybl
	nybl=$var_value
	get_namelist_val nlayers
	nlayers=$var_value

	NPE=`expr $nxbl \* $nybl \* $nlayers`
	if [ "$NPE" = "" ];  then
	   echo "Problem with one of the following parameters.  See nimrod.in"
	   echo "nxbl: " $nxbl
	   echo "nybl: " $nybl
	   echo "nlayers: " $nlayers
	   exit
	fi


	remainder=`expr $NPE % 8`
	if [ "$remainder" = "0" ]; then
		  NNODE=`expr $NPE \/ 8`
		  taskline="tasks_per_node  = 8"
		  envset=""
	else
		  NNODE=`expr $NPE \/ 8 + 1`
		  taskline="total_tasks     = $NPE"
		  envset="unset MP_TASKS_PER_NODE"
	fi

	npe_charged=`expr $NNODE \* 8`

}
#------------------------------------------------------------------
# Given queue and NPE, calculate the maximum length of time we have for
# the queue, and then set NIMROD's run time
#------------------------------------------------------------------
function calc_times
{
	if [ "$CLASS" = "regular" ];  then
	#	if [ "$NNODE" -lt 32 ]; then
	#		set_long_or_short
	#	else
			MT_HOURS=24:00:00
			max_time=86400
	#	fi
	elif [ "$CLASS" = "premium" ];  then
		if [ "$NNODE" -lt 32 ]; then
			MT_HOURS=12:00:00
			max_time=43200
		else
			MT_HOURS=48:00:00
			max_time=172800
		fi
	elif [ "$CLASS" = "low" ];  then
		MT_HOURS=12:00:00
		max_time=43200
	elif [ "$CLASS" = "debug" ];  then
		MT_HOURS=00:30:00
		max_time=4300
	else
		echo "Specify max. residency time manually"
		MT_HOURS=CHANGE_THIS
	fi
}
#-------------------------------------------------------------------
#  Useful info from NERSC
#-------------------------------------------------------------------
#
#                                                          Relative
#        Submit Class     LL Class      Nodes   WallClock  Priority
#        ------------     -----------  -------  ---------  --------
#        interactive ---> interactive    1-  8  00:30:00      1
#        debug ---------> debug          1- 24  00:30:00      2
#
#        premium -----+-> pre_1          1- 31  12:00:00      7
#                     +-> pre_32        32-127  48:00:00      5
#                     +-> pre_128      128-ALL  48:00:00      3
#
#        regular -----+-> reg_1          1- 31  12:00:00      8
#                     +-> reg_1l         1- 31  24:00:00      8
#                     +-> reg_32        32-127  48:00:00      6
#                     +-> reg_128      128-ALL  48:00:00      4
#
#        low -----------> low            1-128  12:00:00      9
#
#
#Job scripts should specify one of interactive, debug, regular, premium
#or low. At submit time these jobs will be directed into one of various
#subclass whose priority is determined by jobs size and wallclock time
#requested.
#
#Jobs requesting 8:00 hours or less wallclock will be allowed to
#complete prior to scheduled system outages. Please checkpoint
#appropriately if you want to use the longer running queues, as jobs
#needing more than 8:00 hour may be terminated for scheduled downtimes.
#
#More information on job limits, aging, and charging available at:
#
#http://hpcf.nersc.gov/running_jobs/ibm/batch_new.html

#General policies:
#
# * Each user may have:
#          o 6 jobs running (this parameter can be adjusted depending on system load).
#          o 10 jobs considered for scheduling (in the idle state)
#          o 30 jobs submitted to the system 
# * Jobs requesting 8:00 hours or less wallclock will be allowed to
#    complete prior to scheduled system outages. Please checkpoint
#    appropriately if you want to use the longer running queues, as jobs
#    needing more than 8:00 hours may be terminated for scheduled
#    downtimes.  
# * Jobs placed on "user hold" (Loadlever status HU) via
#    the llhold command are limited to one week in this status. After
#    one week, these jobs will be removed from the system.  * A 60
#    minute time limit is enforced on all user processes on the login
#    nodes. 

