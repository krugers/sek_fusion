batch_exe=qsub
batch_file="run_batch.pbs"
# Nimrod's time limit=queue_limit - max_time_offset (seconds)
max_time_offset=0	
# I'm assuming that the generic has only one queue and no accounts
class="medium"
class_options="low medium high"
#account=mp21
#account_options="mp21 mp290 mp200 mp94 m275"
#account_desc="mp21=NIMROD mp290=SAIC mp200=UW-M mp94=GA"
#------------------------------------------------------------------
# Set NNODE, NPE, npe_charged
# Need better heuristics based on performance.  See Carls
#------------------------------------------------------------------
function calc_nprocessors 
{
	get_namelist_val nxbl
	nxbl=$var_value
	get_namelist_val nybl
	nybl=$var_value
	get_namelist_val nlayers
	nlayers=$var_value

	NPE=`expr $nxbl \* $nybl \* $nlayers`
	if [ "$NPE" = "" ];  then
	   echo "Problem with one of the following parameters.  See nimrod.in"
	   echo "nxbl: " $nxbl
	   echo "nybl: " $nybl
	   echo "nlayers: " $nlayers
	   exit
	fi


	remainder=`expr $NPE % 8`
	if [ "$remainder" = "0" ]; then
		  NNODE=`expr $NPE \/ 8`
		  envset=""
	elif test $NPE -lt 8; then
		  NNODE=1
		  envset=""
	else
		echo "On oxygen: 8 PE/node (2 Proc/node * 4PE/Proc)"
		echo "Please choose an even number of processors"
		exit
	fi
}
#------------------------------------------------------------------
# Given queue and NPE, calculate the maximum length of time we have for
# the queue, and then set NIMROD's run time
#------------------------------------------------------------------
function calc_times
{
		MT_HOURS=48:00:00
		max_time=172800
}

