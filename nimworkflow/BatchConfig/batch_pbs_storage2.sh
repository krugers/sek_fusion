batch_exe=qsub
batch_file="run_batch.pbs"
# Nimrod's time limit=queue_limit - max_time_offset (seconds)
max_time_offset=0	
# I'm assuming that the generic has only one queue and no accounts
class="opteron"
class_options="opteron txque"
#account=mp21
#account_options="mp21 mp290 mp200 mp94 m275"
#account_desc="mp21=NIMROD mp290=SAIC mp200=UW-M mp94=GA"
#------------------------------------------------------------------
# Set NNODE, NPE, npe_charged
# Need better heuristics based on performance.  See Carls
#------------------------------------------------------------------
function calc_nprocessors 
{
	get_namelist_val nxbl
	nxbl=$var_value
	get_namelist_val nybl
	nybl=$var_value
	get_namelist_val nlayers
	nlayers=$var_value

	NPE=`expr $nxbl \* $nybl \* $nlayers`
	if [ "$NPE" = "" ];  then
	   echo "Problem with one of the following parameters.  See nimrod.in"
	   echo "nxbl: " $nxbl
	   echo "nybl: " $nybl
	   echo "nlayers: " $nlayers
	   exit
	fi


	remainder=`expr $NPE % 2`
	if [ "$remainder" = "0" ]; then
		  NNODE=`expr $NPE \/ 2`
		  envset=""
	else
		echo "On storage2, we have 2 PE/node"
		echo "Please choose an even number of processors"
		exit
	fi
}
#------------------------------------------------------------------
# Given queue and NPE, calculate the maximum length of time we have for
# the queue, and then set NIMROD's run time
#------------------------------------------------------------------
function calc_times
{
		MT_HOURS=48:00:00
		max_time=172800
}


#------------------------------------------------------------------
# From Exby:
# All of the initial opteron nodes (node11-->node22, used for queues  
# opt4, opt8 and opt12)
# and now the new
#  dual-core opteron nodes (node23--->node37 + storage3,  used for  
# queue "emma")
# are using an upgraded openPBS named   "torque" along with the "maui"  
# scheduler.
# 
# for use via storage2, you MUST add
#  /usr/local/torque/bin
# to your $PATH, to gain access to the new qsub  and other pbs  
# commands:  qdel, checkjob, etc.
# 
# Functionally, your PBS scripts which use opt4, opt8 or opt12 will  
# continue to work normally and will get allocated to available  
# nodes11-->22.
# 
# 
# ** The new queue named "emma" has node23-->37+storage3  allocated to  
# it, but currently is reserved for messmer's project deadlines and  
# should not be used for other production runs at this time. **
# 
# 
# I am still investigating some of the advanced features and commands  
# in torque+maui (such as checkjob, canceljob),
# but the common commands such as:   qstat   ,  qdel , qsub ,  pbsnodes
# still work normally.
# 
# As with openPBS, torque is a BATCH system- which means that queued  
# jobs do not instantly start once a previous job finishes.   a 5min  
# delay could occur before a held jobs gets moved to running status.
# 
# Please come see me with questions or if you identify any issues.
# If there is sufficient demand, I can prepare a brown-bag-lunch,  
# Torque talk one day as well.
# 
# Further tweaks to queuing limits or resources are possible within the  
# next week or so, but should not require machine downtime, rather just  
# possible torque+maui daemon restarts as needed.
# 
# Enjoy!
# 
# 
# ::john
