batch_exe=qsub
batch_file="run_batch.pbs"

# Nimrod's time limit=queue_limit - max_time_offset (seconds)
max_time_offset=2500	

# I'm assuming that the generic has only one queue and no accounts
class_options="batch debug"
account_options="FUS013"
account_desc="FUS013=SWIM allocation"
#------------------------------------------------------------------
# Set NNODE, NPE, npe_charged
# Need better heuristics based on performance.  
#------------------------------------------------------------------
function calc_nprocessors 
{
	get_namelist_val nxbl
	nxbl=$var_value
	get_namelist_val nybl
	nybl=$var_value
	get_namelist_val nlayers
	nlayers=$var_value

	NPE=`expr $nxbl \* $nybl \* $nlayers`
	if [ "$NPE" = "" ];  then
	   echo "Problem with one of the following parameters.  See nimrod.in"
	   echo "nxbl: " $nxbl
	   echo "nybl: " $nybl
	   echo "nlayers: " $nlayers
	   exit
	fi

	remainder=`expr $NPE % 2`
	if [ "$remainder" = "0" ]; then
		  NNODE=`expr $NPE \/ 2`
	else
		  NNODE=`expr $NPE \/ 2 + 1`
	fi
}

#------------------------------------------------------------------
# Given queue and NPE, calculate the maximum length of time we have for
# the queue, and then set NIMROD's run time
#------------------------------------------------------------------
function calc_times
{
	if [ "$CLASS" = "batch" ];  then
		if [ "$NNODE" -le 128 ]; then
			MT_HOURS=04:00:00
			max_time=14400
		elif [ "$NNODE" -le 1024 ]; then
			MT_HOURS=12:00:00
			max_time=3686400
		else
			MT_HOURS=24:00:00
			max_time=86400
		fi
	elif [ "$CLASS" = "debug" ];  then
		MT_HOURS=02:00:00
		max_time=8600
	else
		echo "Specify max. residency time manually"
		MT_HOURS=CHANGE_THIS
	fi
}
#-------------------------------------------------------------------
#  Useful info from NCCS
#-------------------------------------------------------------------
# Batch queues
# 
# 
# Queues are used by the batch scheduler to aid in the organization of jobs.
# Users have access to multiple queues, and each queue may allow different job
# limits and have different priorities.
# 
# Unless otherwise notified users have access to two queues:
# 
#    1. Production queue
#      * The batch queue is the standard default queue for production work.
#      * Use
#         o The batch queue should be used for all production runs.
#         o Queue specification is not required to run in the production queue.
#         * Limits
#           o The maximum walltime limit is described in the Scheduling Policies section.
#           o The maximum number of compute nodes is described in the Scheduling Policies section.
#           o If your jobs require resources outside of these limits please
#		complete the relevant request form on the special requests page.
#    2. Debug queue
#       * The debug queue is available for short jobs that require quick
#		turnaround for software development, testing, and debugging.
#           * Use
#             o Jobs submitted to the debug queue have priority over standard production jobs.
#             o Please do not use this queue for production work! Misuse of the
#		debug queue denies other users the ability to develop their software in a
#		timely fashion and unfairly delays other production work. Users who misuse the
#		debug queue may have further access denied.
#                 o Proper use of the debug queue is one job at a time as part
#			of a software development, testing, or debugging cycle.
#                 o Interactive parallel work is an ideal use for the debug
#			queue. See ?Interactive Jobs? for more information.
#                 o To submit a job to the debug queue, you must specify debug using the PBS -q option.
#           * Limits
#                 o The maximum walltime limit is 2 hours.
#                 o There is not a maximum processor limit.
# 
# Scheduling Policies
#    * Jobs are aged 4 minutes per processor compute node requested. Thus, if a
#	100-node job is submitted at the same time as a 400-node job, the system will
#	consider the 400-node job 20 hours older for priority purposes. Accordingly,
#	the 400-node job can be submitted up to 20 hours after the 100-node job and
#	still be considered older for priority purposes.
#
#    * Additionally, jobs are aged based on the queue in which they run. Jobs
#	submitted to the ?debug? queue are considered older than equivalent jobs
#	submitted to the standard queue. This is to allow debug jobs to have a fast
#	turnaround time. The ?debug? queue is for debugging only. Production jobs are
#	not allowed in the ?debug? queue.
#
#    * The ?debug? queue has higher priority than the standard production queue
#	to allow users relatively quick access to debug sessions. To further improve
#	debug job turnaround time, Jaguar reserves 512 compute nodes for the ?debug?
#	queue from noon until 8:00 PM (Eastern Time) each work day. At other times, the
#	?debug? queue maintains its high priority, but the system does not set aside
#	processors for debug-only work. Debug jobs have a maximum walltime of 2 hours.
#
#    * Non-debug jobs of fewer than 128 compute nodes have a maximum walltime of 4 hours.
#
#    * Non-debug jobs using 128 to 1024 compute nodes have a maximum walltime of 12 hours.
#
#    * Non-debug jobs using more than 1024 compute nodes have a maximum walltime of 24 hours.
#
#    * The batch queue system places a limit of two jobs in the ?queued? (i.e.
#	eligible to run) state per user. If a user submits more than two jobs, the
#	additional jobs will enter a ?held? state. Once one of the user?s queued jobs
#	begins execution, one of the held jobs will be moved into the queued state.
#	Note that this is not a limit on the number of jobs that a user may have
#	running simultaneously. Instead, it is a limit on the number eligible to enter
#	a run state.
#
#    * The maximum walltime for any queue is 24 hours.
#
#
