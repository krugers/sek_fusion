#!/usr/bin/env python
import math
from NamelistInput import *
from struct import unpack
from array import array
import numpy
import string
import os

nimhistVars=['istep','t','imode','k','Re Br','Re Bz','Re Bphi','Im Br','Im Bz','Im Bphi']
nimhistVars=nimhistVars+['Re Jr','Re Jz','Re Jphi','Im Jr','Im Jz','Im Jphi']
nimhistVars=nimhistVars+['Re Vr','Re Vz','Re Vphi','Im Vr','Im Vz','Im Vphi']
nimhistVars=nimhistVars+['Re P','Im P','Re Pe','Im Pe','Re n','Im n','Re conc','Im conc']
nimhistVars=nimhistVars+['Re Te','Im Te','Re Ti','Im Ti']

enVars=['istep','t','imode','k','E_m','E_k']
logenVars=enVars
lnsenVars=enVars

disVars=['istep','t','k**2','Total Int E','Ele Int E','Ion Int E','Total E']
disVars=disVars+['ln(Total E)','ln(Int E)','t**(-1)']
disVars=disVars+['I','I','Volt','Total Flux','n=0 Flux']
disVars=disVars+['F','Theta','Wave CFL','Nonlin CFL','Flow CFL']

eq_inputVars=['psi','f','p','q','M','nd','pe','omegeb','kpol','omegp','nhot','thot']

fgnimeqVars=['global_it','ro','zo','rsep','zsep','psio','psix']
fgnimeqVars+=['current','flux','int_e','delstar','resid','nimeq_it']

fgprofsVars=['psifac','rho','f','mu0p','q','mach','nd','mu0pe']
fgprofsVars+=['omegExB','Kpol','omegp','nhot','thot','ti','te','resid']

fluxgridVars=['psifac','f','mu0p','q','mach','nd','mu0pe']
fluxgridVars+=['omegExB','Kpol','omegp','nhot','thot','rho','<j.b/b^2>','resid']

def get_nmodes(nimrodinput):
  """ Find the number of modes given a nimrod input file
  """
  # Make sure we have all the files we need
  a=os.path.exists(nimrodinput)
  if a==False:
     raise Exception(nimrodinput+' was not found in this directory') 

  # Calculate the number of modes to figure out the size to read in
  nimin=NamelistInput([nimrodinput])
  try:
    nonlinval=nimin.variable['nonlinear']
  except KeyError:
    nonlinval='T'
  if "f" in nonlinval.lower():
     lin_nmodes=numpy.int(nimin.variable['lin_nmodes'])
     lin_nmax=numpy.int(nimin.variable['lin_nmax'])
     nmodes=lin_nmodes
     # This requires an understanding of how aranage works
     mode_array=numpy.arange(lin_nmax-lin_nmodes+1,lin_nmax+1)
  else:
    try: 
      nphi=numpy.int(nimin.variable['nphi'])
      nmodes=nphi/3+1
    except KeyError:
      lphi=numpy.int(nimin.variable['lphi'])
      nmodes=2**lphi/3+1     #  numpy is nice
    mode_array=numpy.arange(nmodes)
  return nmodes, mode_array

# nvar=# of variables
# nrps=# of rows per time step
def enShape():
  """ Return nrps,nvar"""
  nmodes, mode_array=get_nmodes("nimrod.in")
  return nmodes, len(enVars)

def disShape(): return 1,len(disVars)

def nimhistShape(): 
  nmodes, mode_array=get_nmodes("nimrod.in")
  return nmodes,len(nimhistVars)

def eq_inputShape(): return 1,len(eq_inputVars)

def fgnimeqShape(): return 1,len(fgnimeqVars)

def fgprofsShape(): return 1,len(fgprofsVars)

def fluxgridShape(): return 1,len(fluxgridVars)

def calculate_shape(nimout):
  """ return number of variables and entries per time step """
  nimMap={'logen': 'enShape','lnsen': 'enShape','en': 'enShape'}
  nimMap['dis']='disShape'
  nimMap['nimhist']='nimhistShape'
  nimMap['eq_input']='eq_inputShape'
  nimMap['fgnimeq']='fgnimeqShape'
  nimMap['fgprofs']='fgprofsShape'
  nimMap['fluxgrid']='fluxgridShape'
  nimMapList=['logen','lnsen','dis','nimhist','en', \
              'eq_input','fgnimeq','fgprofs','fluxgrid']
  notFound=False
  for nimType in nimMapList:
     if nimout.find(nimType)>=0: 
         nimShape=eval(nimMap[nimType])()
         #print 'shape=',nimShape
         return nimType,nimShape[0],nimShape[1]
     else:
         notFound=True
  if notFound: 
     return "NotFound",0,0
     

def getVarList(fileType):
  tempList=eval(fileType+"Vars")
  return tempList[4:]
  

def stripInTime(dat):
  ''' Strip a data array to be monotonic in the first index 
  '''
  totlen=dat.shape[2]
  #print totlen
  rlen=totlen
  for ii in range(totlen-1):
    if dat[0,0,ii] > dat[0,0,ii+1]:
      ih=0
      for ir in range(ii):
        if dat[0,0,ii-ir] == dat[0,0,ii+1]:
          ih=ii-ir
          print dat[0,0,ih],dat[0,0,ii],dat[0,0,ii+1]
          break
      if ih==0:
        print 'time not found'
        print dat[0,0,ii+1],dat[0,0,0]
      else:
        rlen = rlen-(ii-ih+1) 
      print rlen,ih,ii,ii+1

  #print totlen,rlen
  strpdat = numpy.zeros([dat.shape[0],dat.shape[1],rlen])
  il=0
  ih=0
  off=0
  for ii in range(totlen-1):
    if dat[0,0,ii] > dat[0,0,ii+1]:
      for ir in range(ii):
        if dat[0,0,ii-ir] == dat[0,0,ii+1]:
          ih=ii-ir
          break
      print il,ih,il-off,ih-off
      strpdat[:,:,il-off:ih-off] = dat[:,:,il:ih]
      off=off+(ii-ih+1)
      il=ii+1
  #print il,totlen,il-off,totlen-off
  strpdat[:,:,il-off:totlen-off] = dat[:,:,il:totlen]
  return strpdat

def read_bin(binfile,nrps,nvar):
  """ a routine for reading the bin files given the number of rows
  each file has (the whole trick to this"""

  a=os.path.exists(binfile)
  if a==False:
     raise Exception(binfile+' was not found in this directory')

  modeFile=False
  if (binfile=='energy.bin' or binfile=='logen.bin' or \
      binfile=='energy.bin' or binfile=='nimhist.bin'):
    modeFile=True

  # The data is four-bit reals.  Find out how many there are:
  f=open(binfile,'rb');
  nreals=len(f.read())/4
  f.close()
  nvarsize=nvar+2          # Include padding
  #nstepsize=nvarsize*nrps+2  # Include padding
  if (nrps>1 or modeFile):
    nstepsize=nvarsize*nrps+2  # Include padding
  else:
    nstepsize=nvarsize
  nsteps=int(math.floor(nreals/nstepsize))
  if binfile=="eq_input.bin":
    nsteps=nsteps/6 # adjust for spline data
  #print nrps, nvar, nstepsize, nsteps, nvarsize, modeFile

  # Arrays to hold the data.  Numpy uses zero indexing.
  bindata=numpy.zeros( (nvar,nrps,nsteps), numpy.float)

  # Load the data
  f=open(binfile,'rb');
  fmt=">"
  for j in range(nvarsize): fmt=fmt+"f"
  for istep in range(nsteps):
      for i in range(nrps):
          b4=f.read(nvarsize*4)
          fields=unpack(fmt,b4)
          for j in range(1,nvar+1):
             bindata[j-1,i,istep]=fields[j] # strip padding
      if (nrps>1 or modeFile):
          b4=f.read(8) # padding
      #b4=f.read(8) # padding
  f.close()

  bindata = stripInTime(bindata)
  return bindata

def read_txt(txtfile,nrps,nvar):
  """ a routine for reading the txt files given the number of rows
  each file has (the whole trick to this"""

  a=os.path.exists(txtfile)
  if a==False:
     raise Exception(binfile+' was not found in this directory')

  # Read by line
  f=open(txtfile,'r');
  nlines=numpy.int(len(f.read().strip().split("\n")))
  f.close()
  nsteps=int(nlines/nrps)
  #print nsteps,nlines,nrps,nvar

  # Arrays to hold the data.  Numpy uses zero indexing.
  tmpdata=numpy.zeros( (nvar,nrps,nsteps), numpy.float)

  # Load the data
  f=open(txtfile,'r');
  istep=-1
  iuse=0
  iline=0
  for istep in range(nsteps):
    jj=0
    while not jj>=nrps and not iline>nlines:
      llist=f.readline().split()
      iline=iline+1
      if len(llist)>nrps-1:
        for ii in range(nvar):
          tmpdata[ii,jj,iuse]=numpy.float(llist[ii])
        if jj>0 and not tmpdata[0,jj,iuse]==tmpdata[0,0,iuse]:
          print 'read error at istep=',istep,jj,iline
          print tmpdata[:,0,iuse]
          print tmpdata[:,jj,iuse]
          raise Exception('file read error')
        jj=jj+1
    iuse=iuse+1
    if iuse>nsteps:
      print 'at limit:',istep,iuse

  # reset to exclude blank lines
  nsteps=iuse
  txtdata=numpy.zeros( (nvar,nrps,nsteps), numpy.float)
  txtdata[:,:,0:nsteps]=tmpdata[:,:,0:nsteps]

  f.close()

  txtdata = stripInTime(txtdata)
  return txtdata

def dataPrint(data):
  """ Pretty print the data """
  nvar,nrps,nsteps=data.shape
  for istep in range(nsteps):
    for j in range(nrps):
      for i in range(nvar): print data[i,j,istep],
      print "\n",


def read_nimout(outfile):
  """ Routine that acts as a wrapper to other routines in the file:
      pass in the file, and get back the data"""

  a=os.path.exists(outfile)
  if a==False:
     raise Exception(outfile+' was not found in this directory')

  # Get the data from the file
  extension=os.path.splitext(outfile)[1]
  fileType,nrps,nvar=calculate_shape(outfile)
  if extension==".bin":
    data=read_bin(outfile,nrps,nvar)
  elif extension==".txt":
    data=read_txt(outfile,nrps,nvar)
  else:
    raise Exception("File extension not recognized: "+extension)
    return
  return fileType,data


