###
##  Useful aliases
#

# Repository aliases
alias myusage='getnim -U $USER'
alias mp21='getnim -R mp21'
alias mp290='getnim -R mp290'
alias mp21hog='getnim -Rmp21 -L | sort -k2,2nr'
 
#
alias   q='llq '
alias superq='llq -l -u $USER'
alias qme="llqs | grep $USER"
alias qlow="llqs | grep low | nl | more"
alias qdebug="llqs | grep debug | nl | more"
alias qregular="llqs | grep reg_ | grep -v reg_1l | nl | more"
alias qregularl="llqs | grep reg_1l | nl | more"
alias qpremium="llqs | grep premium | nl | more" 
alias qr1="llqs | grep reg_1 | grep -v reg_1l | nl | more"
alias qr1l="llqs | grep reg_1l | nl | more"
alias qr32="llqs | grep reg_32 | nl | more"
alias qr128="llqs | grep reg_12 | nl | more"
alias qp1="llqs | grep pre_1 | nl | more"
alias qp32="llqs | grep pre_32 | nl | more"
alias qp128="llqs | grep pre_12 | nl | more"
alias qcountr="llqs | grep reg_ | wc"
alias qcountp="llqs | grep pre_ | wc"
alias qcountl="llqs | grep low | wc"
alias qmer="llqs | grep reg_ | grep -v reg_1l | nl | grep $USER; qcountr"
alias qmerl="llqs | grep reg_1l | nl | grep $USER; qcountr"
alias qmep="llqs | grep pre_ | nl | grep $USER; qcountp"
alias qmel="llqs | grep low | nl | grep $USER; qcountl"
alias qm3d="llqs | egrep 'hstrauss|jbreslau|u4207|u4434'"
alias qnimrod="llqs | egrep 'skruger|brennan|schnack|u2230|eheld'"

module load GNU
module load superlu_dist/2.0_64
module load lapack/3.0
module load hdf5_64/1.6.4
