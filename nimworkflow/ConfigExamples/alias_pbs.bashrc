alias q='qstat -a'
alias qme="qstat -a | grep $USER"
alias qlow="qstat -a | grep low | nl | more"
alias qdebug="qstat -a | grep debug | nl | more"
alias qregular="qstat -a | grep batch | nl | more"

alias myusage showusage

# showq
# =====
# The MOAB utility ?showq? can be used to view a more detailed description of
#  the queue. The utility will display the queue in the following states:
#   1. active   - These jobs are currently running.
#   2. eligible - These jobs are currently queued awaiting resources. A user is allowed two jobs in eligible state.
#   3. blocked  - These jobs are currently queued, but are not eligible to run.
# 		Common reasons for jobs in this state are jobs on hold and the owning user
# 		currently has two jobs in the eligible state.
# 
# checkjob
# ========
# The MOAB utility ?checkjob? can be used to view details of a job in the
# queue. For example, if job 736 is a job currently in the queue in a blocked
# state, the following can be used to view why the job is in a blocked state:
# 
# >checkjob 736
# 
# The return may contain a line similar to the following: BlockMsg: job 736
# violates idle HARD MAXJOB limit of 2 for user <userid> (Req: 1 InUse: 2)
# 
# This line indicates the job is in the blocked state because owning user has
# reached the limit of two job currently in the eligible state.
# 
# showstart
# =========
# The MOAB utility ?showstart? can be used to view an estimated start time for a given job. For example,
# 
# > showstart 736
#  job 736 requires 2048 procs for 1:00:00:00
#  
#  Estimated Rsv based start in           3:41:18 on Tue March 1 19:21:18
#  Estimated Rsv based completion in   1:03:41:18 on Wed March 2 19:21:18
#  
#  Best Partition: jaguar
#  
# > 
# 
# xtshowcabs
# ==========
# The utility ?xtshowcabs? can be used to see what jobs are currently running and more importantly where.
# 
