module load xt-libsci python/2.5.2 

PGI_MOD=`module list 2>&1 | grep PrgEnv-pgi`
if [ -n "$PGI_MOD" ] ; then
  echo "Switching to Pathscale Environment"
  module swap PrgEnv-pgi PrgEnv-pathscale
fi
module load netcdf-pathscale/3.6.2
