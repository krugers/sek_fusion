#
# Batch system aliases
#alias qstat     'qs '
#alias   q       ' qstat | cut -c1-50,74-90 | more'
alias   q       ' qstat |  more'
alias qa       ' qstat | cut -c1-50,74-90'
alias qme      "qstat -u $USER | cut -c1-50,74-90"
alias qlow "qstat | grep low | nl | more"
alias qdebug "qstat | grep debug | nl | more"
alias qregular "qstat | grep reg_ | grep -v reg_1l | grep -v 'H reg' |  nl | more"
alias qpremium "qstat | grep premium | nl | more" 
alias qregular "qstat | grep reg_ | grep small | grep -v 'H reg' |  nl | more"
alias qcountr    "qstat | grep reg_ | grep -v 'H reg' | wc"
alias qcountp     "qstat | grep pre_ | wc"
alias qcountl     "qstat | grep low | wc"
alias qmer      "qstat | grep reg_ | grep -v reg_1l | nl | grep $USER; qcountr"
alias qmep      "qstat | grep pre_ | nl | grep $USER; qcountp"
alias qmel      "qstat | grep low | nl | grep $USER; qcountl"

alias myusage showusage

###
##  Other useful commands.
#
# apstat
# The apstat command gives the number of up nodes and idle nodes. Also a list
# of current pending and running jobs. apstat -r command displays all the nodes
# reservations. Please see apstat man page for more information.
#
## showq
# =====
# The MOAB utility ?showq? can be used to view a more detailed description of
#  the queue. The utility will display the queue in the following states:
#   1. active   - These jobs are currently running.
#   2. eligible - These jobs are currently queued awaiting resources. A user is allowed two jobs in eligible state.
#   3. blocked  - These jobs are currently queued, but are not eligible to run.
# 		Common reasons for jobs in this state are jobs on hold and the owning user
# 		currently has two jobs in the eligible state.
# 
# checkjob
# ========
# The MOAB utility ?checkjob? can be used to view details of a job in the
# queue. For example, if job 736 is a job currently in the queue in a blocked
# state, the following can be used to view why the job is in a blocked state:
# 
# >checkjob 736
# 
# The return may contain a line similar to the following: BlockMsg: job 736
# violates idle HARD MAXJOB limit of 2 for user <userid> (Req: 1 InUse: 2)
# 
# This line indicates the job is in the blocked state because owning user has
# reached the limit of two job currently in the eligible state.
# 
# showstart
# =========
# The MOAB utility ?showstart? can be used to view an estimated start time for a given job. For example,
# 
# > showstart 736
#  job 736 requires 2048 procs for 1:00:00:00
#  
#  Estimated Rsv based start in           3:41:18 on Tue March 1 19:21:18
#  Estimated Rsv based completion in   1:03:41:18 on Wed March 2 19:21:18
#  
#  Best Partition: jaguar
#  
# > 
# 
# xtshowcabs
# ==========
# The utility ?xtshowcabs? can be used to see what jobs are currently running and more importantly where.
# 
